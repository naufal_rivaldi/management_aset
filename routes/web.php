<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ====================================================
// Auth - route
// ====================================================
Auth::routes();
Route::post('/login', 'Auth\LoginController@login')->name('login.auth');
Route::get('/logout', 'Auth\AuthController@logout')->name('auth.logout');
// ====================================================

Route::get('cari-aset/{param}', 'CariController@index')->name('cari.aset');

Route::group(['middleware' => 'auth'], function(){
    // ====================================================
    // User - route
    // ====================================================
    Route::group(['prefix' => 'user'], function(){
        Route::get('/ubah-password', 'UserController@ubahPassword')->name('user.ubah-password');
        Route::post('/ubah-password', 'UserController@storeUbahPassword')->name('user.ubah-password.store');
    });
    // ====================================================

    // ====================================================
    // Dashboard - route
    // ====================================================
    Route::get('/', 'DashboardController@index')->name('dashboard.index');
    // ====================================================

    // ====================================================
    // Master - route
    // ====================================================
    Route::name('master.')->prefix('master')->group(function(){
        Route::group(['middleware' => 'level:1'], function(){
            // ====================================================
            // User - route
            // ====================================================
            Route::group(['prefix' => 'user'], function(){
                Route::get('json/{param}', 'Master\UserController@json')->name('user.json');
                Route::get('active/{param}', 'Master\UserController@active')->name('user.active');
                Route::get('inactive/{param}', 'Master\UserController@inactive')->name('user.inactive');
                Route::put('reset/{param}', 'Master\UserController@resetPassword')->name('user.reset');
            });
            Route::resource('user', 'Master\UserController');
            // ====================================================

            // ====================================================
            // Departemen - route
            // ====================================================
            Route::group(['prefix' => 'departemen'], function(){
                Route::get('json/{param}', 'Master\DepartemenController@json')->name('departemen.json');
            });
            Route::resource('departemen', 'Master\DepartemenController');
            // ====================================================
        });

        // ====================================================
        // Aset - route
        // ====================================================
        Route::group(['prefix' => 'aset'], function(){
            Route::get('json/{param}', 'Master\AsetController@json')->name('aset.json');
            Route::put('active/{param}', 'Master\AsetController@active')->name('aset.active');
            Route::put('inactive/{param}', 'Master\AsetController@inactive')->name('aset.inactive');
            Route::get('qrdownload/{param}', 'Master\AsetController@qrdownload')->name('aset.qrdownload');
        });
        Route::resource('aset', 'Master\AsetController');
        // ====================================================
    });
    // ====================================================

    // ====================================================
    // Fitur - route
    // ====================================================
    Route::name('fitur.')->prefix('fitur')->group(function(){
        Route::group(['middleware' => 'level:2'], function(){
            // ====================================================
            // Aset - route
            // ====================================================
            Route::group(['prefix' => 'aset'], function(){
                Route::get('json/{param}', 'Fitur\AsetController@json')->name('aset.json');
                Route::get('data/{param}', 'Fitur\AsetController@ubahData')->name('aset.ubah.data');
            });
            Route::resource('aset', 'Fitur\AsetController');
            // ====================================================
        });

        Route::group(['middleware' => 'level:1,2,3'], function(){
            // ====================================================
            // Transaksi - route
            // ====================================================
            Route::group(['prefix' => 'transaksi'], function(){
                Route::get('json/{param}', 'Fitur\TransaksiController@json')->name('transaksi.json');
                Route::put('verifikasi/{param}', 'Fitur\TransaksiController@verifikasi')->name('transaksi.verifikasi');
                Route::get('get/aset/{param}', 'Fitur\TransaksiController@getAset')->name('transaksi.get.aset');
                Route::get('check/aset/{param}', 'Fitur\TransaksiController@checkAset')->name('transaksi.check.aset');
            });
            Route::resource('transaksi', 'Fitur\TransaksiController');
            // ====================================================
        });
    });
    // ====================================================

    // ====================================================
    // Laporan - route
    // ====================================================
    Route::name('laporan.')->prefix('laporan')->group(function(){
        Route::group(['middleware' => 'level:1'], function(){
            // ====================================================
            // Aset - route
            // ====================================================
            Route::group(['prefix' => 'aset'], function(){
                Route::get('/', 'Laporan\LaporanAsetController@index')->name('aset.index');
                Route::get('json/{param}', 'Laporan\LaporanAsetController@json')->name('aset.json');
            });
            // ====================================================

            // ====================================================
            // Mutasi - route
            // ====================================================
            Route::group(['prefix' => 'mutasi'], function(){
                Route::get('/', 'Laporan\LaporanMutasiController@index')->name('mutasi.index');
                Route::get('json/{param}', 'Laporan\LaporanMutasiController@json')->name('mutasi.json');
            });
            // ====================================================
        });
    });
    // ====================================================
});
