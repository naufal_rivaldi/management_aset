@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h5>List Mutasi</h5>
      </div>
      <div class="card-body">
        <!-- Datatable -->
        <div class="table-responsive">
          <table id="myDataTable" class="table table-bordered table-striped">
            <thead class="thead-light">
              <tr>
                <th>Tanggal</th>
                <th data-type="select" data-filtering='{!! parseJson($filtering->verifikasi) !!}'>Verifikasi</th>
                <th>Awal Departemen</th>
                <th>Akhir Departemen</th>
                <th>List Aset</th>
                <th data-type="select" data-filtering='{!! parseJson($filtering->status) !!}'>Status</th>
              </tr>
            </thead>
            <tfoot class="tfoot-light">
              <tr>
                <th>Tanggal</th>
                <th>Verifikasi</th>
                <th>Awal Departemen</th>
                <th>Akhir Departemen</th>
                <th>List Aset</th>
                <th>Status</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- Datatable -->
      </div>
    </div>
  </div>
</div>
@endsection

@push('script')
<script>
  var app = new Vue({
    el: '#app',
    data: {
      table: '',
    },
    methods:{
      // =============================
      // Datatable init
      // =============================
      datatables: function(){
        let _this = this;

        // Clone row on head table
        $('#myDataTable thead tr').clone(true).appendTo('#myDataTable thead');
        
        // Make column search on head table clone column
        $('#myDataTable thead tr:eq(1) th').each( function (i) {
          var title = $(this).text();
          var type = $(this).data('type');
          
          if(title != 'Action'){
            switch (type) {
              case 'select':
                var data = $(this).data('filtering');

                var text = '<select class="form-control">';
                text += '<option value="">Pilih '+ title +'</option>';
                $.each(data, function(index, val){
                  text += '<option value="'+ val +'">'+ val +'</option>';
                });
                text += '</select>';

                $(this).html(text);

                $('select', this).on('change', function(){
                  if(_this.table.column(i).search() !== this.value){
                    _this.table.column(i).search(this.value).draw();
                  }
                });
                break;
            
              default:
                $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );

                $('input', this).on('keyup change', function () {
                  if (_this.table.column(i).search() !== this.value) {
                    _this.table.column(i).search(this.value).draw();
                  }
                });
                break;
            }
          }else{
            $(this).html('');
          }
          
        });

        this.table = $('#myDataTable').DataTable({
          order: [[0, "desc"]],
          dom: 'lBfrtip',
          buttons: [
            'csv', 'excel', 'pdf'
          ],
          processing: true,
          serverSide: true,
          orderCellsTop: true,
          fixedHeader: true,
          paging: true,
          lengthChange: true,
          searching: true,
          ordering: true,
          info: true,
          scrollX: true,
          autoWidth: true,
          responsive: true,
          ajax: {
            type: "GET",
            url: "{{ route('laporan.mutasi.json', 'datatable') }}",
            data: {},
          },
          columns: [
            {data: 'tanggal_transaksi', name: 'tanggal_transaksi', defaultContent: '-'},
            {data: 'verifikasi_status', name: 'verifikasi_status', defaultContent: '-'},
            {data: 'departemen_awal.nama', name: 'departemen_awal.nama', defaultContent: '-'},
            {data: 'departemen_akhir.nama', name: 'departemen_akhir.nama', defaultContent: '-'},
            {data: 'list_aset', name: 'list_aset', defaultContent: '-'},
            {data: 'status', name: 'status', defaultContent: '-'},
          ],
          columnDefs: [
            {
                render: function (data, type, full, meta) {
                    return "<div class='text-wrap width-200'>" + data + "</div>";
                },
                targets: 4
            }
          ]
        });
      },
    },
    mounted: function(){
      let _this = this;
      this.datatables();
    }
  })
</script>
@endpush