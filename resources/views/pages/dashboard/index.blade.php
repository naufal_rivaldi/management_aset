@extends('layouts.app')

@section('content')
<div class="row">
  @if(Auth::user()->level == 1)
  <div class="col-md-3">
    <div class="widget widget-stat">
      <div class="media">
        <div class="media-left media-middle">
          <i class="fa fa-user icon-transparent-area custom-color-green"></i>
        </div>
        <div class="media-body">
          <span class="title">Jumlah User</span>
          <span class="value">{{ count($user) }}</span>
        </div>
      </div>
      <p class="footer text-success">
        <span>Management Aset</span>
      </p>
    </div>
  </div>
  <div class="col-md-3">
    <div class="widget widget-stat">
      <div class="media">
        <div class="media-left media-middle">
          <i class="ti-truck icon-transparent-area custom-color-red"></i>
        </div>
        <div class="media-body">
          <span class="title">Jumlah Departemen</span>
          <span class="value">{{ count($departemen) }}</span>
        </div>
      </div>
      <p class="footer text-success">
        <span>Management Aset</span>
      </p>
    </div>
  </div>
  @endif

  @if(Auth::user()->level == 1 || Auth::user()->departemen->nama = 'Accounting')
  <div class="col-md-3">
    <div class="widget widget-stat">
      <div class="media">
        <div class="media-left media-middle">
          <i class="ti-harddrives icon-transparent-area custom-color-purple"></i>
        </div>
        <div class="media-body">
          <span class="title">Jumlah Aset</span>
          <span class="value">{{ count($aset) }}</span>
        </div>
      </div>
      <p class="footer text-success">
        <span>Management Aset</span>
      </p>
    </div>
  </div>
  @endif

  <div class="col-md-3">
    <div class="widget widget-stat">
      <div class="media">
        <div class="media-left media-middle">
          <i class="ti-control-shuffle icon-transparent-area custom-color-blue"></i>
        </div>
        <div class="media-body">
          <span class="title">Verifikasi Transaksi</span>
          <span class="value">{{ UserHelper::checkTransaksi() }}</span>
        </div>
      </div>
      <p class="footer text-success">
        <span>Management Aset</span>
      </p>
    </div>
  </div>

  @if(Auth::user()->level == 2)
  <div class="col-md-3">
    <div class="widget widget-stat">
      <div class="media">
        <div class="media-left media-middle">
          <i class="ti-harddrives icon-transparent-area custom-color-yellow"></i>
        </div>
        <div class="media-body">
          <span class="title">Data Aset Accounting</span>
          <span class="value">{{ UserHelper::checkDataAset() }}</span>
        </div>
      </div>
      <p class="footer text-success">
        <span>Management Aset</span>
      </p>
    </div>
  </div>
  @endif
</div>
@endsection