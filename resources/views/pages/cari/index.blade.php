<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      <!-- Datatables core css -->
    <link href="{{ asset('assets/plugins/datatables.net-bs4/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Datatables extensions css -->
    <link href="{{ asset('assets/plugins/datatables.net-buttons-bs4/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/datatables.net-colreorder-bs4/colreorder.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Toestr Js -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <!-- App css -->
    <link href="{{ asset('assets/css/bootstrap-custom.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    <title>Cari Aset</title>
  </head>
  <body>
    <div class="container">
      <h1>Detail Aset</h1>
      <hr>

      <div class="row">
        <div class="col-md-4">
          <div class="widget widget-stat">
            <div class="media">
              <div class="media-left media-middle">
                <i class="fa fa-money icon-transparent-area custom-color-green"></i>
              </div>
              <div class="media-body">
                <span class="title">Harga</span>
                <span class="value">Rp. {{ $aset->harga ? number_format($aset->harga) : '-' }}</span>
              </div>
            </div>
            <p class="footer text-success">
              <span>Harga beli aset.</span>
            </p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="widget widget-stat">
            <div class="media">
              <div class="media-left media-middle">
                <i class="fa fa-credit-card icon-transparent-area custom-color-red"></i>
              </div>
              <div class="media-body">
                <span class="title">Nomer Bukti</span>
                <span class="value">{{ $aset->no_bukti ? $aset->no_bukti : '-' }}</span>
              </div>
            </div>
            <p class="footer text-success">
              <span>Nomer bukti / invoice pembayaran aset.</span>
            </p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="widget widget-stat">
            <div class="media">
              <div class="media-left media-middle">
                <i class="fa fa-line-chart icon-transparent-area custom-color-purple"></i>
              </div>
              <div class="media-body">
                <span class="title">Umur Aset</span>
                <span class="value">{{ $aset->umur ? $aset->umur : '-' }} Tahun</span>
              </div>
            </div>
            <p class="footer text-success">
              <span>Sebagai pembanding penyusutan aset.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-5">
          <div class="card">
            <div class="card-header">
              <h5>Data Aset</h5>
            </div>
            <div class="card-body">
              <b>Kode</b>
              <p>{{ $aset->kode }}</p>
              <hr>
              <b>Merk</b>
              <p>{{ $aset->merk }}</p>
              <hr>
              <b>Nama</b>
              <p>{{ $aset->nama }}</p>
              <hr>
              <b>Serial</b>
              <p>{{ $aset->serial }}</p>
              <hr>
              <b>Tanggal Perolehan</b>
              <p>{{ longDateFormat($aset->tanggal_perolehan) }}</p>
              <hr>
              <b>Kondisi</b>
              <p>{{ ucwords($aset->kondisi) }}</p>
              <hr>
              <b>Status</b>
              <p>{!! status($aset->status) !!}</p>
              <hr>
              <b>Warehouse</b>
              <p>{{ $aset->departemen->nama }}</p>
              <hr>
              <b>Deskripsi</b>
              <p>{{ $aset->deskripsi }}</p>
            </div>
          </div>
        </div>

        <div class="col-md-7">
          <div class="card">
            <div class="card-header">
              <h5><i class="fa fa-line-chart text-success"></i> Preview Penyusutan Aset</h5>
            </div>
            <div class="card-body">
              @if($aset->harga === null || $aset->nilai_residu === null || $aset->umur === null)
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                  Data belum lengkap, silahkan hubungi accounting untuk info lebih lanjut.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              @else
                <!-- Datatable -->
                <div class="table-responsive">
                  <table id="myDataTable" class="table table-bordered table-striped">
                    <thead class="thead-light">
                      <tr>
                        <th>Tahun</th>
                        <th>Jumlah Bulan</th>
                        <th>Penyusutan</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php $total = 0 @endphp
                      @foreach($depresiasi as $row)
                        <tr>
                          <th>{{ $row->year }}</th>
                          <th>{{ $row->month }}</th>
                          <th>Rp. {{ number_format($row->depresiasi) }}</th>
                        </tr>
                        @php $total += $row->depresiasi @endphp
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th colspan="2" class="text-center">Total Penyusutan</th>
                        <th>Rp. {{ number_format($total) }}</th>
                      </tr>
                      <tr>
                        <th colspan="2" class="text-center">Nilai Residu</th>
                        <th>Rp. {{ number_format($aset->nilai_residu) }}</th>
                      </tr>
                      <tr>
                        <th colspan="2" class="text-center">Total</th>
                        <th>Rp. {{ number_format($total + $aset->nilai_residu) }}</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- Datatable -->
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Vendor -->
    <script src="{{ asset('assets/js/vendor.min.js') }}"></script>

    <!-- Vue js -->
    <script src="{{ asset('js/vue.js') }}"></script>

    <!-- Datables Core -->
    <script src="{{ asset('assets/plugins/datatables.net/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables.net-bs4/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Datables Extension -->
    <script src="{{ asset('assets/plugins/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables.net-buttons-bs4/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables.net-colreorder/dataTables.colReorder.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables.net-colreorder-bs4/colReorder.bootstrap4.min.js') }}"></script>

    <!-- Sweetalert 2 -->
    <script src="{{ asset('js/plugin/sweetalert2.js') }}"></script>

    <!-- Toestr Js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <!-- Datatables Init -->
    <script src="{{ asset('assets/js/pages/tables-dynamic.init.min.js') }}"></script>

    <!-- App -->
    <script src="{{ asset('assets/js/app.min.js') }}"></script>
  </body>
</html>