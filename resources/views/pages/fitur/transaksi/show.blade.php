@extends('layouts.app')

@push('css')
<!-- Select2 css -->
<link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('fitur.transaksi.index') }}" class="btn btn-success">
          <i class="ti-arrow-circle-left"></i> Kembali
        </a>

        @if(Auth::user()->level == 1)
          <a href="{{ route('fitur.transaksi.edit', $transaksi->id) }}" class="btn btn-info">
            <i class="ti-pencil-alt"></i> Ubah Data
          </a>
        @endif
      </div>
      
      <div class="card-body">
        <h2>Transaksi <br><small>Mutasi barang</small></h2>
        <div class="mt-4">
          <h5><span class="badge badge-primary">{{ longDateFormat($transaksi->tanggal_transaksi) }}</span></h5>
          <div class="row">
            <div class="col-md-6">
              <div class="row">
                <div class="col-sm-6">
                  <h5>Dari <i class="ti-arrow-circle-right"></i></h5>
                  <p>
                    {{ $transaksi->departemen_awal->nama }}
                    <hr>
                    <i class="ti-themify-favicon"></i> {{ $transaksi->departemen_awal->no_telp }}<br>
                    <i class="ti-location-pin"></i> {{ $transaksi->departemen_awal->alamat }}
                  </p>
                </div>
                <div class="col-sm-6">
                  <h5><i class="ti-arrow-circle-left"></i> Penerima</h5>
                  <p>{{ $transaksi->departemen_akhir->nama }}</p>
                  <hr>
                  <i class="ti-themify-favicon"></i> {{ $transaksi->departemen_akhir->no_telp }}<br>
                  <i class="ti-location-pin"></i> {{ $transaksi->departemen_akhir->alamat }}
                </div>
              </div>

              <div class="content1 mt-3">
                <b>Kondisi</b>
                <p>{{ $transaksi->kondisi }}</p>
                <b>Status</b>
                <p>{!! statusAset($transaksi->status) !!}</p>
                <b>Pembuat</b>
                <p>{{ $transaksi->user->nama }}</p>
                <b>Verifikasi Status</b>
                <p>{!! statusVerifikasi($transaksi->verifikasi_status) !!}</p>
              </div>
            </div>

            <div class="col-md-6">
              <h5>List Aset</h5>
              <div class="alert alert-warning alert-dismissible fade show" role="alert">
                Jika ada kesalahan data mohon hubungi kantor pusat <b>(General Affair)</b>.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode</th>
                      <th>Nama</th>
                      <th>Kondisi</th>
                      <th>Check Aset</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $no = 1; @endphp
                    @foreach($transaksi->transaksi_detail as $row)
                      <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $row->aset->kode }}</td>
                        <td>{{ $row->aset->nama }}</td>
                        <td>{{ ucwords($row->aset->kondisi) }}</td>
                        <td><input type="checkbox" name="check_aset[]" class="check-aset" value="{{ $row->id }}" {{ $row->status == 1 ? 'checked' : '' }} {{ $transaksi->verifikasi_status == 1 || $transaksi->departemen_awal->id == Auth::user()->departemen_id ? 'disabled' : '' }}></td>
                        <td>
                          <button class="btn btn-info btn-sm" @click="getAset({{ $row->aset->id }})" data-toggle="modal" data-target="#modalAset"><i class="ti-eye"></i></button>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      @if(Auth::user()->departemen->id == $transaksi->akhir_departemen_id && $transaksi->verifikasi_status == 0)
      <div class="card-footer text-right">
        <button class="btn btn-primary" @click="verifikasi({{ $transaksi->id }})"><i class="ti-check-box"></i> Verifikasi</button>
      </div>
      @endif
    </div>
  </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="modalAset" tabindex="-1" aria-labelledby="modalAsetLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalAsetLabel">Detail Aset</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <b>Kode</b>
        <p class="aset-kode">@{{ aset.kode }}</p>
        <hr>
        <b>Merk</b>
        <p class="aset-merk">@{{ aset.merk }}</p>
        <hr>
        <b>Nama</b>
        <p class="aset-nama">@{{ aset.nama }}</p>
        <hr>
        <b>Serial</b>
        <p class="aset-serial">@{{ aset.serial }}</p>
        <hr>
        <b>Tanggal Perolehan</b>
        <p class="aset-tanggal-perolehan">@{{ aset.tanggal_perolehan }}</p>
        <hr>
        <b>Kondisi</b>
        <p class="aset-kondisi">@{{ aset.kondisi }}</p>
        <hr>
        <b>Warehouse</b>
        <p class="aset-departemen">@{{ aset.departemen.nama }}</p>
        <hr>
        <b>Deskripsi</b>
        <p class="aset-deskripsi">@{{ aset.deskripsi }}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

@push('script')
<!-- Select2 Plugin -->
<script src="{{ asset('assets/plugins/select2/select2.min.js') }}"></script>

<!-- Select2 Init -->
<script src="{{ asset('assets/js/pages/forms-select2.init.js') }}"></script>

<script>
  var app = new Vue({
    el: '#app',
    data: {
      aset: {
        departemen: {}
      }
    },
    methods:{
      getAset(aset_id){
        let _this = this;
        $.ajax({
          url: "{{ route('fitur.transaksi.get.aset', ':id') }}".replace(":id", aset_id),
          type: "GET",
          dataType: "JSON",
          success: function(response){
            _this.aset = response;
          }
        })
      },

      checkAset(type, id){
        $.ajax({
          url: "{{ route('fitur.transaksi.check.aset', ':id') }}".replace(":id", id),
          type: "GET",
          data: {
            type: type
          },
          success: function(response){
            toastr.success(response.message);
          }
        });
      },

      verifikasi(id){
        Swal.fire({
          title: 'Apakah anda yakin ingin verifikasi?',
          text: "Data akan dimasukkan kedalam warehouse yang sesuai.",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Verifikasi',
          cancelButtonText: 'Batal',
        }).then((result) => {
          if (result.value) {
            $.ajax({
              url: "{{ route('fitur.transaksi.verifikasi', ':id') }}".replace(":id", id),
              type: "PUT",
              data: {
                _token: "{{ csrf_token() }}"
              },
              success: function(response){
                if(response.status){
                  location.reload();
                }else{
                  toastr.warning(response.message);
                }
              }
            });
          }
        })
      }
    },
    mounted(){
      let _this = this;

      // ===================================
      // Check Aset
      // ===================================
      $(document).on('click', '.check-aset', function(){
        let val = $(this).val();
        if(this.checked){
          _this.checkAset(1, val);
        }else{
          _this.checkAset(0, val);
        }
      });
      // ===================================
    }
  })
</script>
@endpush