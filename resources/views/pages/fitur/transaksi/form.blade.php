@extends('layouts.app')

@push('css')
<!-- Select2 css -->
<link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet">
<!-- Summernote -->
<link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}">
@endpush

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('fitur.transaksi.index') }}" class="btn btn-success">
          <i class="ti-arrow-circle-left"></i> Kembali
        </a>
      </div>
      <form action="{{ $transaksi->id ? route('fitur.transaksi.update', $transaksi->id) : route('fitur.transaksi.store') }}" method="POST">
        @csrf
        @if($transaksi->id)
          @method('PUT')
        @endif
        <div class="card-body">
          <!-- Form -->
          <div class="form-group">
            <label for="">Tanggal Transaksi <span class="text-danger">*</span></label>
            <input type="date" name="tanggal_transaksi" class="form-control col-md-6 {{ $errors->has('tanggal_transaksi') ? 'is-invalid' : '' }}" value="{{ old('tanggal_transaksi') ? old('tanggal_transaksi') : $transaksi->tanggal_transaksi }}">

            <!-- Error -->
            @if($errors->has('tanggal_transaksi'))
              <small class="text-danger">
                {{ $errors->first('tanggal_transaksi') }}
              </small>
            @endif
          </div>

          <div class="row">
            <div class="col-sm-5">
              <label for="">Asal Tempat Mutasi <span class="text-danger">*</span></label>
              <select name="awal_departemen_id" id="awal_departemen_id" class="form-control select2 {{ $errors->has('awal_departemen_id') ? 'is-invalid' : '' }}">
                <option value="">Pilih Asal Tempat Mutasi</option>
                @foreach($departemens as $departemen)
                  <option value="{{ $departemen->id }}" {{ old('awal_departemen_id') == $departemen->id || $transaksi->awal_departemen_id == $departemen->id ? 'selected' : '' }}>{{ $departemen->nama }}</option>
                @endforeach
              </select>

              <!-- Error -->
              @if($errors->has('awal_departemen_id'))
                <small class="text-danger">
                  {{ $errors->first('awal_departemen_id') }}
                </small>
              @endif
            </div>
            
            <div class="col-sm-1 text-center" style="padding-top: 2%">
              <i class="ti-control-shuffle"></i>
            </div>

            <div class="col-sm-5">
              <label for="">Akhir Tempat Mutasi <span class="text-danger">*</span></label>
              <select name="akhir_departemen_id" id="akhir_departemen_id" class="form-control select2 {{ $errors->has('akhir_departemen_id') ? 'is-invalid' : '' }}">
                <option value="">Pilih Akhir Tempat Mutasi</option>
                @foreach($departemens as $departemen)
                  <option value="{{ $departemen->id }}" {{ old('akhir_departemen_id') == $departemen->id || $transaksi->akhir_departemen_id == $departemen->id ? 'selected' : '' }}>{{ $departemen->nama }}</option>
                @endforeach
              </select>

              <!-- Error -->
              @if($errors->has('akhir_departemen_id'))
                <small class="text-danger">
                  {{ $errors->first('akhir_departemen_id') }}
                </small>
              @endif
            </div>
          </div>
          <br>

          <div class="row">
            <div class="col-sm-5">
              <label for="">Pilih Aset</label>
              <select name="aset" id="aset" class="form-control select2 {{ $errors->has('aset') ? 'is-invalid' : '' }}">
                <option value="">Pilih Aset</option>
              </select>
              
              <small class="text-info">Pilih asal tempat mutasi terlebih dahulu.</small><br>
            </div>
            
            <div class="col-sm-1 text-center" style="padding-top: 2%">
              <i class="ti-control-shuffle"></i>
            </div>

            <div class="col-sm-5">
              <label for="">List Aset Mutasi <span class="text-danger">*</span></label>
              <select name="aset_id[]" class="form-control aset_id" id="aset_id" style="height: 200px" id="" multiple>
                @if($transaksi->id != '')
                  @foreach($transaksi->transaksi_detail as $row)
                    <option value="{{ $row->aset_id }}">{{ $row->aset->kode.'-'.$row->aset->nama }}</option>
                  @endforeach
                @endif
              </select>
              <!-- <div class="mt-2">
                <input type="checkbox" name="check_all_aset" value="1"> Select all aset?
              </div> -->

              <!-- Error -->
              @if($errors->has('aset_id'))
                <small class="text-danger">
                  {{ $errors->first('aset_id') }}
                </small>
              @endif
            </div>
          </div>
          <br>

          <div class="form-group">
            <label for="">Status <span class="text-danger">*</span></label>
            <select name="status" id="status" class="form-control col-md-6 {{ $errors->has('status') ? 'is-invalid' : '' }}">
              <option value="">Pilih Status</option>
              <option value="1" {{ old('status') == '1' || $transaksi->status == '1' ? 'selected' : '' }}>Baru</option>
              <option value="2" {{ old('status') == '2' || $transaksi->status == '2' ? 'selected' : '' }}>Bekas</option>
            </select>

            <!-- Error -->
            @if($errors->has('status'))
              <small class="text-danger">
                {{ $errors->first('status') }}
              </small>
            @endif
          </div>
          
          <div class="form-group">
            <label for="">Deskripsi</label>
            <textarea name="deskripsi" id="deskripsi" rows="5" class="form-control summernote {{ $errors->has('deskripsi') ? 'is-invalid' : '' }}">{{ old('deskripsi') ? old('deskripsi') : $transaksi->deskripsi }}</textarea>

            <!-- Error -->
            @if($errors->has('deskripsi'))
              <small class="text-danger">
                {{ $errors->first('deskripsi') }}
              </small>
            @endif
          </div>
          <!-- Form -->
        </div>

        <div class="card-footer">
          <button type="submit" class="btn btn-primary submit">
            <i class="ti-save"></i> {{ $transaksi->id ? 'Update' : 'Simpan' }}
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('script')
<!-- Select2 Plugin -->
<script src="{{ asset('assets/plugins/select2/select2.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>

<script>
  var app = new Vue({
    el: '#app',
    data: {
      
    },
    methods:{
      addAsetValue(){
        let $departemen_id = $('#awal_departemen_id').val();
        $.ajax({
          url: "{{ route('fitur.transaksi.json', 'aset') }}",
          type: "GET",
          dataType: "JSON",
          data: {
            departemen_id: $departemen_id
          },
          success: function(result){
            let $aset = $('#aset');
            $aset.empty();
            
            // fill the data
            $aset.append('<option value="">Pilih Aset</option>');
            $.each(result, function(index, val){
              $aset.append('<option value="'+val.id+'">'+val.kode+'-'+val.nama+'</option>');
            });
          }
        });
      }
    },
    mounted(){
      let _this = this;
      // ========================================
      // Run Function event
      // ========================================
      @if($transaksi->id != '')
        _this.addAsetValue();
      @endif
      // ========================================

      // ========================================
      // Init Library
      // ========================================
      $('.select2').select2();
      $('.summernote').summernote({
        height: 300
      });
      // ========================================

      // ========================================
      // Function event
      // ========================================
      $('#awal_departemen_id').on('change', function(){
        $('#aset_id').empty();
        _this.addAsetValue();
      });

      $('#aset').on('change', function(){
        let arrayValue = [];
        let $text = $('#aset option:selected').html();
        let $val = $(this).val();

        $('#aset_id > option').each(function(){
          arrayValue.push(this.value);
        });
        
        if($val != '' && arrayValue.includes($val) == false){
          $('#aset_id').append('<option value="'+$val+'">'+$text+'</option>');
        }
      });

      $('#aset_id').on('click', function(){
        $.each($(this).val(), function(index, val){
          $("#aset_id option[value='"+val+"']").remove();
        });
      });

      $('.submit').on('click', function(){
        $('#aset_id > option').prop('selected', true);
      });
      // ========================================
    }
  })
</script>
@endpush