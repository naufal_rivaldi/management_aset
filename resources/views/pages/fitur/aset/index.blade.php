@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h5>Data Aset Tersimpan <span class="badge badge-secondary">Latest</span></h5>
      </div>
      <div class="card-body">
        <!-- Datatable -->
        <div class="table-responsive">
          <table id="" class="table table-bordered table-striped myDataTable1">
            <thead class="thead-light">
              <tr>
                <th>Kode</th>
                <th>Nama</th>
                <th data-type="select" data-filtering='{!! parseJson($filtering->kondisi) !!}'>Kondisi</th>
                <th>Warehouse</th>
                <th>Action</th>
              </tr>
            </thead>
          </table>
        </div>
        <!-- Datatable -->
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h5>Data Aset Masuk <span class="badge badge-success">New</span></h5>
      </div>
      <div class="card-body">
        <div class="alert alert-info alert-dismissible fade show" role="alert">
          Data dapat diakses oleh accounting
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <!-- Datatable -->
        <div class="table-responsive">
          <table id="" class="table table-bordered table-striped myDataTable2">
            <thead class="thead-light">
              <tr>
                <th>Kode</th>
                <th>Nama</th>
                <th data-type="select" data-filtering='{!! parseJson($filtering->kondisi) !!}'>Kondisi</th>
                <th>Warehouse</th>
                <th>Action</th>
              </tr>
            </thead>
          </table>
        </div>
        <!-- Datatable -->
      </div>
    </div>
  </div>
</div>
@endsection

@push('script')
<script>
  var app = new Vue({
    el: '#app',
    data: {
      table: '',
    },
    methods:{
      // =============================
      // Datatable init
      // =============================
      datatables: function(idx, type){
        let _this = this;

        // Clone row on head table
        $('.myDataTable'+idx+' thead tr').clone(true).appendTo('.myDataTable'+idx+' thead');
        
        // Make column search on head table clone column
        $('.myDataTable'+idx+' thead tr:eq(1) th').each( function (i) {
          var title = $(this).text();
          var type = $(this).data('type');
          
          if(title != 'Action'){
            switch (type) {
              case 'select':
                var data = $(this).data('filtering');

                var text = '<select class="form-control">';
                text += '<option value="">Pilih '+ title +'</option>';
                $.each(data, function(index, val){
                  text += '<option value="'+ val +'">'+ val +'</option>';
                });
                text += '</select>';

                $(this).html(text);

                $('select', this).on('change', function(){
                  if(_this.table.column(i).search() !== this.value){
                    _this.table.column(i).search(this.value).draw();
                  }
                });
                break;
            
              default:
                $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );

                $('input', this).on('keyup change', function () {
                  if (_this.table.column(i).search() !== this.value) {
                    _this.table.column(i).search(this.value).draw();
                  }
                });
                break;
            }
          }else{
            $(this).html('');
          }
          
        });

        this.table = $('.myDataTable'+idx).DataTable({
          order: [[0, "desc"]],
          processing: true,
          serverSide: true,
          orderCellsTop: true,
          fixedHeader: true,
          paging: true,
          lengthChange: true,
          searching: true,
          ordering: true,
          info: true,
          scrollX: true,
          autoWidth: true,
          responsive: true,
          ajax: {
            type: "GET",
            url: "{{ route('fitur.aset.json', ':type') }}".replace(':type', type),
            data: {},
          },
          columns: [
            {data: 'kode', name: 'kode', defaultContent: '-'},
            {data: 'nama', name: 'nama', defaultContent: '-'},
            {data: 'kondisi', name: 'kondisi', defaultContent: '-'},
            {data: 'departemen.nama', name: 'departemen.nama', defaultContent: '-'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
        });
      },

      deleteData: function(id){
        let _this = this;
        let link = "{{ route('fitur.aset.destroy', ':id') }}";
        link = link.replace(':id', id);

        $.ajax({
          type: "DELETE",
          url: link,
          data: {
            _token: "{{ csrf_token() }}"
          },
          success: function(){
            _this.table.ajax.reload();
          }
        });
      },
    },
    mounted: function(){
      let _this = this;
      this.datatables(1, 'datatable1');
      this.datatables(2, 'datatable2');

      $(document).on('click', '.delete-data', function(){
        let id = $(this).data('id');
        
        Swal.fire({
          title: 'Apakah anda yakin?',
          text: "Data akan terhapus secara permanen",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Hapus',
          cancelButtonText: 'Batal',
        }).then((result) => {
          if (result.value) {
            _this.deleteData(id);  
          }
        })
      });
    }
  })
</script>
@endpush