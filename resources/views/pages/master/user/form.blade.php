@extends('layouts.app')

@push('css')
<!-- Select2 css -->
<link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('master.user.index') }}" class="btn btn-success">
          <i class="ti-arrow-circle-left"></i> Kembali
        </a>
      </div>
      <form action="{{ $user->id ? route('master.user.update', $user->id) : route('master.user.store') }}" method="POST">
        @csrf
        @if($user->id)
          @method('PUT')
        @endif
        <div class="card-body">
          <!-- Form -->
          <div class="form-group">
            <label for="">PIC <span class="text-danger">*</span></label>
            <input type="text" name="pic" class="form-control col-md-6 {{ $errors->has('pic') ? 'is-invalid' : '' }}" value="{{ old('pic') ? old('pic') : $user->pic }}">

            <!-- Error -->
            @if($errors->has('pic'))
              <small class="text-danger">
                {{ $errors->first('pic') }}
              </small>
            @endif
          </div>
          
          <div class="form-group">
            <label for="">Email <span class="text-danger">*</span></label>
            <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email') ? old('email') : $user->email }}">

            <!-- Error -->
            @if($errors->has('email'))
              <small class="text-danger">
                {{ $errors->first('email') }}
              </small>
            @endif
          </div>

          <div class="form-group">
            <label for="">Level <span class="text-danger">*</span></label>
            <select name="level" id="level" class="form-control col-md-6 {{ $errors->has('level') ? 'is-invalid' : '' }}">
              <option value="">Pilih level</option>
              <option value="1" {{ old('level') == '1' || $user->level == '1' ? 'selected' : '' }}>Admin</option>
              <option value="2" {{ old('level') == '2' || $user->level == '2' ? 'selected' : '' }}>Editor</option>
              <option value="3" {{ old('level') == '3' || $user->level == '3' ? 'selected' : '' }}>Pengguna</option>
            </select>

            <!-- Error -->
            @if($errors->has('level'))
              <small class="text-danger">
                {{ $errors->first('level') }}
              </small>
            @endif
          </div>

          @if($user->id)
            <div class="form-group">
              <label for="">Status <span class="text-danger">*</span></label>
              <select name="status" id="status" class="form-control col-md-6 {{ $errors->has('status') ? 'is-invalid' : '' }}">
                <option value="">Pilih status</option>
                <option value="1" {{ old('status') == '1' || $user->status == '1' ? 'selected' : '' }}>Active</option>
                <option value="2" {{ old('status') == '0' || $user->status == '0' ? 'selected' : '' }}>Inactive</option>
              </select>

              <!-- Error -->
              @if($errors->has('level'))
                <small class="text-danger">
                  {{ $errors->first('level') }}
                </small>
              @endif
            </div>
          @endif

          <div class="form-group">
            <label for="">Departemen <span class="text-danger">*</span></label><br>
            <select name="departemen_id" id="departemen_id" class="form-control select2 col-md-6 {{ $errors->has('departemen_id') ? 'is-invalid' : '' }}">
              <option value="">Pilih Departemen</option>
              @foreach($departemens as $departemen)
                <option value="{{ $departemen->id }}" {{ old('departemen_id') == $departemen->id || $user->departemen_id == $departemen->id ? 'selected' : '' }}>{{ $departemen->nama }}</option>
              @endforeach
            </select>

            <!-- Error -->
            <br>
            @if($errors->has('departemen_id'))
              <small class="text-danger">
                {{ $errors->first('departemen_id') }}
              </small>
            @endif
          </div>
          <!-- Form -->
        </div>

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">
            <i class="ti-save"></i> {{ $user->id ? 'Update' : 'Simpan' }}
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('script')
<!-- Select2 Plugin -->
<script src="{{ asset('assets/plugins/select2/select2.min.js') }}"></script>

<script>
  var app = new Vue({
    el: '#app',
    data: {
      
    },
    methods:{
      
    },
    mounted(){
      $('.select2').select2();
    }
  })
</script>
@endpush