@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('master.departemen.index') }}" class="btn btn-success">
          <i class="ti-arrow-circle-left"></i> Kembali
        </a>
      </div>
      <form action="{{ $departemen->id ? route('master.departemen.update', $departemen->id) : route('master.departemen.store') }}" method="POST">
        @csrf
        @if($departemen->id)
          @method('PUT')
        @endif
        <div class="card-body">
          <!-- Form -->
          <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nama" class="form-control {{ $errors->has('nama') ? 'is-invalid' : '' }}" value="{{ old('nama') ? old('nama') : $departemen->nama }}">

            <!-- Error -->
            @if($errors->has('nama'))
              <small class="text-danger">
                {{ $errors->first('nama') }}
              </small>
            @endif
          </div>

          <div class="form-group">
            <label for="">No Telepon</label>
            <input type="text" name="no_telp" class="form-control {{ $errors->has('no_telp') ? 'is-invalid' : '' }}" value="{{ old('no_telp') ? old('no_telp') : $departemen->no_telp }}">

            <!-- Error -->
            @if($errors->has('no_telp'))
              <small class="text-danger">
                {{ $errors->first('no_telp') }}
              </small>
            @endif
          </div>

          <div class="form-group">
            <label for="">Alamat</label>
            <textarea name="alamat" id="alamat" rows="5" class="form-control {{ $errors->has('alamat') ? 'is-invalid' : '' }}">{{ old('alamat') ? old('alamat') : $departemen->alamat }}</textarea>

            <!-- Error -->
            @if($errors->has('alamat'))
              <small class="text-danger">
                {{ $errors->first('alamat') }}
              </small>
            @endif
          </div>
          <!-- Form -->
        </div>

        <div class="card-footer text-right">
          <button type="submit" class="btn btn-primary">
            <i class="ti-save"></i> {{ $departemen->id ? 'Update' : 'Simpan' }}
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('script')
<script>
  var app = new Vue({
    el: '#app',
    data: {
      
    },
    methods:{
      
    }
  })
</script>
@endpush