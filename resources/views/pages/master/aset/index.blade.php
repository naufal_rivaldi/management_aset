@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      @if(Auth::user()->level == 1)
        <div class="card-header text-right">
          <a href="{{ route('master.aset.create') }}" class="btn btn-success">
            <i class="ti-plus"></i> Tambah data aset
          </a>
        </div>
      @else
        <div class="card-header">
          <h5>List Aset</h5>
        </div>
      @endif
      <div class="card-body">
        <!-- Datatable -->
        <div class="table-responsive">
          <table id="myDataTable" class="table table-bordered table-striped">
            <thead class="thead-light">
              <tr>
                <th>Kode</th>
                <th>Nama</th>
                <th data-type="select" data-filtering='{!! parseJson($filtering->kondisi) !!}'>Kondisi</th>
                <th data-type="select" data-filtering='{!! parseJson($filtering->status) !!}'>Status</th>
                <th>Warehouse</th>
                <th>Action</th>
              </tr>
            </thead>
          </table>
        </div>
        <!-- Datatable -->
      </div>
    </div>
  </div>
</div>
@endsection

@push('script')
<script>
  var app = new Vue({
    el: '#app',
    data: {
      table: '',
    },
    methods:{
      // =============================
      // Datatable init
      // =============================
      datatables: function(){
        let _this = this;

        // Clone row on head table
        $('#myDataTable thead tr').clone(true).appendTo('#myDataTable thead');
        
        // Make column search on head table clone column
        $('#myDataTable thead tr:eq(1) th').each( function (i) {
          var title = $(this).text();
          var type = $(this).data('type');
          
          if(title != 'Action'){
            switch (type) {
              case 'select':
                var data = $(this).data('filtering');

                var text = '<select class="form-control">';
                text += '<option value="">Pilih '+ title +'</option>';
                $.each(data, function(index, val){
                  text += '<option value="'+ val +'">'+ val +'</option>';
                });
                text += '</select>';

                $(this).html(text);

                $('select', this).on('change', function(){
                  if(_this.table.column(i).search() !== this.value){
                    _this.table.column(i).search(this.value).draw();
                  }
                });
                break;
            
              default:
                $(this).html( '<input type="text" class="form-control" placeholder="Search '+title+'" />' );

                $('input', this).on('keyup change', function () {
                  if (_this.table.column(i).search() !== this.value) {
                    _this.table.column(i).search(this.value).draw();
                  }
                });
                break;
            }
          }else{
            $(this).html('');
          }
          
        });

        this.table = $('#myDataTable').DataTable({
          order: [[0, "desc"]],
          processing: true,
          serverSide: true,
          orderCellsTop: true,
          fixedHeader: true,
          paging: true,
          lengthChange: true,
          searching: true,
          ordering: true,
          info: true,
          scrollX: true,
          autoWidth: true,
          responsive: true,
          ajax: {
            type: "GET",
            url: "{{ route('master.aset.json', 'datatable') }}",
            data: {},
          },
          columns: [
            {data: 'kode', name: 'kode', defaultContent: '-'},
            {data: 'nama', name: 'nama', defaultContent: '-'},
            {data: 'kondisi', name: 'kondisi', defaultContent: '-'},
            {data: 'status', name: 'status', defaultContent: '-'},
            {data: 'departemen.nama', name: 'departemen.nama', defaultContent: '-'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
        });
      },

      active: function(id){
        let _this = this;
        let link = "{{ route('master.aset.active', ':id') }}";
        link = link.replace(':id', id);

        $.ajax({
          type: "PUT",
          url: link,
          data: {
            _token: "{{ csrf_token() }}"
          },
          success: function(){
            _this.table.ajax.reload();
          }
        });
      },

      inactive: function(id){
        let _this = this;
        let link = "{{ route('master.aset.inactive', ':id') }}";
        link = link.replace(':id', id);

        $.ajax({
          type: "PUT",
          url: link,
          data: {
            _token: "{{ csrf_token() }}"
          },
          success: function(){
            _this.table.ajax.reload();
          }
        });
      },

      deleteData: function(id){
        let _this = this;
        let link = "{{ route('master.aset.destroy', ':id') }}";
        link = link.replace(':id', id);

        $.ajax({
          type: "DELETE",
          url: link,
          data: {
            _token: "{{ csrf_token() }}"
          },
          success: function(){
            _this.table.ajax.reload();
          }
        });
      },
    },
    mounted: function(){
      let _this = this;
      this.datatables();

      // ========================================================
      // Active data on datatable
      // ========================================================
      $(document).on('click', '.btn-active', function(){
        let id = $(this).data('id');
        
        Swal.fire({
          title: 'Aktifkan data?',
          text: "Data akan kembali aktif",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Aktifkan',
          cancelButtonText: 'Batal',
        }).then((result) => {
          if (result.value) {
            _this.active(id);
          }
        })
      });
      // ========================================================

      // ========================================================
      // Inactive data on datatable
      // ========================================================
      $(document).on('click', '.btn-inactive', function(){
        let id = $(this).data('id');
        
        Swal.fire({
          title: 'Nonaktifkan data?',
          text: "Data akan menjadi nonaktif",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Nonaktifkan',
          cancelButtonText: 'Batal',
        }).then((result) => {
          if (result.value) {
            _this.inactive(id);
          }
        })
      });
      // ========================================================

      // ========================================================
      // Delete data on datatable
      // ========================================================
      $(document).on('click', '.delete-data', function(){
        let id = $(this).data('id');
        
        Swal.fire({
          title: 'Apakah anda yakin?',
          text: "Data akan terhapus secara permanen",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Hapus',
          cancelButtonText: 'Batal',
        }).then((result) => {
          if (result.value) {
            _this.deleteData(id);  
          }
        })
      });
      // ========================================================
    }
  })
</script>
@endpush