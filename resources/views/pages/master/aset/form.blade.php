@extends('layouts.app')

@push('css')
<!-- Select2 css -->
<link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('master.aset.index') }}" class="btn btn-success">
          <i class="ti-arrow-circle-left"></i> Kembali
        </a>
      </div>
      <form action="{{ $aset->id ? route('master.aset.update', $aset->id) : route('master.aset.store') }}" method="POST">
        @csrf
        @if($aset->id)
          @method('PUT')
        @endif
        <div class="card-body">
          <!-- Form -->
          <div class="form-group">
            <label for="">Kode <span class="text-danger">*</span></label>
            <input type="text" name="kode" class="form-control col-md-6 {{ $errors->has('kode') ? 'is-invalid' : '' }}" value="{{ old('kode') ? old('kode') : $aset->kode }}">

            <!-- Error -->
            @if($errors->has('kode'))
              <small class="text-danger">
                {{ $errors->first('kode') }}
              </small>
            @endif
          </div>

          <div class="form-group">
            <label for="">Merk</label>
            <input type="text" name="merk" class="form-control col-md-6 {{ $errors->has('merk') ? 'is-invalid' : '' }}" value="{{ old('merk') ? old('merk') : $aset->merk }}">

            <!-- Error -->
            @if($errors->has('merk'))
              <small class="text-danger">
                {{ $errors->first('merk') }}
              </small>
            @endif
          </div>

          <div class="form-group">
            <label for="">Nama <span class="text-danger">*</span></label>
            <input type="text" name="nama" class="form-control {{ $errors->has('nama') ? 'is-invalid' : '' }}" value="{{ old('nama') ? old('nama') : $aset->nama }}">

            <!-- Error -->
            @if($errors->has('nama'))
              <small class="text-danger">
                {{ $errors->first('nama') }}
              </small>
            @endif
          </div>

          <div class="form-group">
            <label for="">Serial</label>
            <input type="text" name="serial" class="form-control {{ $errors->has('serial') ? 'is-invalid' : '' }}" value="{{ old('serial') ? old('serial') : $aset->serial }}">

            <!-- Error -->
            @if($errors->has('serial'))
              <small class="text-danger">
                {{ $errors->first('serial') }}
              </small>
            @endif
          </div>

          <div class="form-group">
            <label for="">Tanggal Perolehan <span class="text-danger">*</span></label>
            <input type="date" name="tanggal_perolehan" class="form-control col-md-6 {{ $errors->has('tanggal_perolehan') ? 'is-invalid' : '' }}" value="{{ old('tanggal_perolehan') ? old('tanggal_perolehan') : $aset->tanggal_perolehan }}">

            <!-- Error -->
            @if($errors->has('tanggal_perolehan'))
              <small class="text-danger">
                {{ $errors->first('tanggal_perolehan') }}
              </small>
            @endif
          </div>

          <div class="form-group">
            <label for="">Kondisi <span class="text-danger">*</span></label>
            <select name="kondisi" id="kondisi" class="form-control col-md-6 {{ $errors->has('kondisi') ? 'is-invalid' : '' }}">
              <option value="">Pilih Kondisi</option>
              <option value="baru" {{ old('kondisi') == 'baru' || $aset->kondisi == 'baru' ? 'selected' : '' }}>Baru</option>
              <option value="bekas" {{ old('kondisi') == 'bekas' || $aset->kondisi == 'bekas' ? 'selected' : '' }}>Bekas</option>
            </select>

            <!-- Error -->
            @if($errors->has('kondisi'))
              <small class="text-danger">
                {{ $errors->first('kondisi') }}
              </small>
            @endif
          </div>

          <div class="form-group">
            <label for="">Warehouse <span class="text-danger">*</span></label><br>
            <select name="departemen_id" id="departemen_id" class="form-control select2 col-md-6 {{ $errors->has('departemen_id') ? 'is-invalid' : '' }}">
              <option value="">Pilih Warehouse</option>
              @foreach($departemens as $departemen)
                <option value="{{ $departemen->id }}" {{ old('departemen_id') == $departemen->id || $aset->departemen_id == $departemen->id ? 'selected' : '' }}>{{ $departemen->nama }}</option>
              @endforeach
            </select>

            <!-- Error -->
            <br>
            @if($errors->has('departemen_id'))
              <small class="text-danger">
                {{ $errors->first('departemen_id') }}
              </small>
            @endif
          </div>

          <div class="form-group">
            <label for="">Deskripsi</label>
            <textarea name="deskripsi" id="deskripsi" rows="5" class="form-control {{ $errors->has('deskripsi') ? 'is-invalid' : '' }}">{{ old('deskripsi') ? old('deskripsi') : $aset->deskripsi }}</textarea>

            <!-- Error -->
            @if($errors->has('deskripsi'))
              <small class="text-danger">
                {{ $errors->first('deskripsi') }}
              </small>
            @endif
          </div>
          <!-- Form -->
        </div>

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">
            <i class="ti-save"></i> {{ $aset->id ? 'Update' : 'Simpan' }}
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('script')
<!-- Select2 Plugin -->
<script src="{{ asset('assets/plugins/select2/select2.min.js') }}"></script>

<script>
  var app = new Vue({
    el: '#app',
    data: {
      
    },
    methods:{
      
    },
    mounted(){
      $('.select2').select2();
    }
  })
</script>
@endpush