@extends('layouts.app')

@push('css')
<!-- Select2 css -->
<link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endpush

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <a href="{{ route('master.aset.index') }}" class="btn btn-success">
          <i class="ti-arrow-circle-left"></i> Kembali
        </a>
      </div>
    </div>
  </div>
</div>

@if($aset->aset_check == 0)
  <div class="alert alert-info alert-dismissible fade show" role="alert">
    Data perlu diupdate oleh departemen accounting.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif

<div class="row">
  <div class="col-md-3">
    <div class="widget widget-stat">
      <div class="media">
        <div class="media-left media-middle">
          {!! QRCode::text(route('cari.aset', $aset->kode))->svg(); !!}
        </div>
        <div class="media-body">
          <span class="title">Qr Code</span>
          <span class="value">Scan Here</span>
          <small>Untuk melihat detail aset</small><br>
          <a href="{{ asset('img/qr-code/'.$aset->kode.'.png') }}" class="btn btn-primary btn-sm" downlaod>Download</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-3">
    <div class="widget widget-stat">
      <div class="media">
        <div class="media-left media-middle">
          <i class="fa fa-money icon-transparent-area custom-color-green"></i>
        </div>
        <div class="media-body">
          <span class="title">Harga</span>
          <span class="value">Rp. {{ $aset->harga ? number_format($aset->harga) : '-' }}</span>
        </div>
      </div>
      <p class="footer text-success">
        <span>Harga beli aset.</span>
      </p>
    </div>
  </div>
  <div class="col-md-3">
    <div class="widget widget-stat">
      <div class="media">
        <div class="media-left media-middle">
          <i class="fa fa-credit-card icon-transparent-area custom-color-red"></i>
        </div>
        <div class="media-body">
          <span class="title">Nomer Bukti</span>
          <span class="value">{{ $aset->no_bukti ? $aset->no_bukti : '-' }}</span>
        </div>
      </div>
      <p class="footer text-success">
        <span>Nomer bukti / invoice pembayaran aset.</span>
      </p>
    </div>
  </div>
  <div class="col-md-3">
    <div class="widget widget-stat">
      <div class="media">
        <div class="media-left media-middle">
          <i class="fa fa-line-chart icon-transparent-area custom-color-purple"></i>
        </div>
        <div class="media-body">
          <span class="title">Umur Aset</span>
          <span class="value">{{ $aset->umur ? $aset->umur : '-' }} Tahun</span>
        </div>
      </div>
      <p class="footer text-success">
        <span>Sebagai pembanding penyusutan aset.</span>
      </p>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-5">
    <div class="card">
      <div class="card-header">
        <h5>Data Aset</h5>
      </div>
      <div class="card-body">
        <b>Kode</b>
        <p>{{ $aset->kode }}</p>
        <hr>
        <b>Merk</b>
        <p>{{ $aset->merk }}</p>
        <hr>
        <b>Nama</b>
        <p>{{ $aset->nama }}</p>
        <hr>
        <b>Serial</b>
        <p>{{ $aset->serial }}</p>
        <hr>
        <b>Tanggal Perolehan</b>
        <p>{{ longDateFormat($aset->tanggal_perolehan) }}</p>
        <hr>
        <b>Kondisi</b>
        <p>{{ ucwords($aset->kondisi) }}</p>
        <hr>
        <b>Status</b>
        <p>{!! status($aset->status) !!}</p>
        <hr>
        <b>Warehouse</b>
        <p>{{ $aset->departemen->nama }}</p>
        <hr>
        <b>Deskripsi</b>
        <p>{{ $aset->deskripsi }}</p>
      </div>
    </div>
  </div>

  <div class="col-md-7">
    <div class="card">
      <div class="card-header">
        <h5><i class="fa fa-line-chart text-success"></i> Preview Penyusutan Aset</h5>
      </div>
      <div class="card-body">
        @if($aset->harga === null || $aset->nilai_residu === null || $aset->umur === null)
          <div class="alert alert-info alert-dismissible fade show" role="alert">
            Data belum lengkap, silahkan hubungi accounting untuk info lebih lanjut.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @else
          <!-- Datatable -->
          <div class="table-responsive">
            <table id="myDataTable" class="table table-bordered table-striped">
              <thead class="thead-light">
                <tr>
                  <th>Tahun</th>
                  <th>Jumlah Bulan</th>
                  <th>Penyusutan</th>
                </tr>
              </thead>
              <tbody>
                @php $total = 0 @endphp
                @foreach($depresiasi as $row)
                  <tr>
                    <th>{{ $row->year }}</th>
                    <th>{{ $row->month }}</th>
                    <th>Rp. {{ number_format($row->depresiasi) }}</th>
                  </tr>
                  @php $total += $row->depresiasi @endphp
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th colspan="2" class="text-center">Total Penyusutan</th>
                  <th>Rp. {{ number_format($total) }}</th>
                </tr>
                <tr>
                  <th colspan="2" class="text-center">Nilai Residu</th>
                  <th>Rp. {{ number_format($aset->nilai_residu) }}</th>
                </tr>
                <tr>
                  <th colspan="2" class="text-center">Total</th>
                  <th>Rp. {{ number_format($total + $aset->nilai_residu) }}</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- Datatable -->
        @endif
      </div>
    </div>
  </div>
</div>
@endsection

@push('script')
<!-- Select2 Plugin -->
<script src="{{ asset('assets/plugins/select2/select2.min.js') }}"></script>

<!-- Select2 Init -->
<script src="{{ asset('assets/js/pages/forms-select2.init.js') }}"></script>

<script>
  var app = new Vue({
    el: '#app',
    data: {
      table: '',
    },
    methods:{

    },
    mounted(){
      let _this = this; 
    }
  })
</script>
@endpush