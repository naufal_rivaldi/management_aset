@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <a href="{{ url()->previous() }}" class="btn btn-success">
          <i class="ti-arrow-circle-left"></i> Kembali
        </a>
      </div>
      <div class="card-body">
        <form action="{{ route('user.ubah-password.store') }}" method="POST">
          @csrf
          <div class="form-group">
            <label for="">Password Lama <span class="text-danger">*</span></label>
            <input type="password" name="old_password" class="form-control {{ $errors->has('old_password') ? 'is-invalid' : '' }}" value="{{ old('old_password') }}" autofocus>

            <!-- Error -->
            @if($errors->has('old_password'))
              <small class="text-danger">
                {{ $errors->first('old_password') }}
              </small>
            @endif
          </div>
          <div class="form-group">
            <label for="">Password Baru <span class="text-danger">*</span></label>
            <input type="password" name="new_password" class="form-control {{ $errors->has('new_password') ? 'is-invalid' : '' }}" value="{{ old('new_password') }}">

            <!-- Error -->
            @if($errors->has('new_password'))
              <small class="text-danger">
                {{ $errors->first('new_password') }}
              </small>
            @endif
          </div>
          <div class="form-group">
            <label for="">Confirmasi Password <span class="text-danger">*</span></label>
            <input type="password" name="confirm_password" class="form-control {{ $errors->has('confirm_password') ? 'is-invalid' : '' }}" value="{{ old('confirm_password') }}">

            <!-- Error -->
            @if($errors->has('confirm_password'))
              <small class="text-danger">
                {{ $errors->first('confirm_password') }}
              </small>
            @endif
          </div>

          <button type="submit" class="btn btn-primary">
            <i class="ti-save"></i> Simpan
          </button>
          <button type="reset" class="btn btn-danger">
            <i class="ti-reload"></i> Batal
          </button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection