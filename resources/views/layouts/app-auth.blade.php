<!doctype html>
<html lang="en" class="fullscreen-bg">
<head>
  <title>Login | {{ env('APP_NAME') }}</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

  <!-- jQuery UI -->
  <link href="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- App css -->
  <link href="{{ asset('assets/css/bootstrap-custom.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

  <!-- Favicon -->
  <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">

  @stack('css')
</head>
<body>
  <!-- WRAPPER -->
  <div id="wrapper" class="d-flex align-items-center justify-content-center">
    <div class="auth-box ">
      <div class="left">
        <div class="content">
          @yield('content')
        </div>
      </div>
      <div class="right">
        <div class="overlay"></div>
        <div class="content text">
          <h1 class="heading">Sistem Informasi Pengelolaan Aset</h1>
          <p>PT. Citra Warna Jaya Abadi<br>By Kadek Bayu Arimbawa</p>
        </div>
      </div>
    </div>
  </div>
  <!-- END WRAPPER -->

  <!-- Script -->
  <!-- Vendor -->
  <script src="{{ asset('assets/js/vendor.min.js') }}"></script>

  <!-- jQuery UI -->
  <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
  
  @stack('js')

  <!-- App -->
  <script src="{{ asset('assets/js/app.min.js') }}"></script>
</body>
</html>