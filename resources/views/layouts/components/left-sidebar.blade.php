<div id="sidebar-nav" class="sidebar">
  <nav>
    <ul class="nav" id="sidebar-nav-menu">
      <!-- Menu - Main -->
      <li class="menu-group">Main</li>
      <li><a href="{{ route('dashboard.index') }}" class="{{ Route::current()->getName() == 'dashboard.index' ? 'active' : '' }}"><i class="ti-dashboard"></i> <span class="title">Dashboard</span></a></li>
      <!-- Menu - Main -->


      @if(Auth::user()->level == 1)
        <!-- Menu - Master -->
        <li class="menu-group">Master</li>
        <li><a href="{{ route('master.user.index') }}" class="{{ Route::current()->getName() == 'master.user.index' || Route::current()->getName() == 'master.user.create' || Route::current()->getName() == 'master.user.edit' ? 'active' : '' }}"><i class="ti-user"></i> <span class="title">User</span></a></li>
        <li><a href="{{ route('master.departemen.index') }}" class="{{ Route::current()->getName() == 'master.departemen.index' || Route::current()->getName() == 'master.departemen.create' || Route::current()->getName() == 'master.departemen.edit' ? 'active' : '' }}"><i class="ti-map-alt"></i> <span class="title">Departemen</span></a></li>
      @endif
      <li><a href="{{ route('master.aset.index') }}" class="{{ Route::current()->getName() == 'master.aset.index' || Route::current()->getName() == 'master.aset.create' || Route::current()->getName() == 'master.aset.edit' || Route::current()->getName() == 'master.aset.show' ? 'active' : '' }}"><i class="ti-harddrives"></i> <span class="title">Aset</span></a></li>

      <!-- Menu - Feature -->
      <li class="menu-group">Fitur</li>
      @if(Auth::user()->level == 2)
        <li><a href="{{ route('fitur.aset.index') }}" class="{{ Route::current()->getName() == 'fitur.aset.index' || Route::current()->getName() == 'fitur.aset.show' ? 'active' : '' }}"><i class="ti-harddrives"></i> <span class="title">Data Aset</span> <span class="badge badge-warning">{{ UserHelper::checkDataAset() }}</span></a></li>
      @endif
      <li>
        <a href="{{ route('fitur.transaksi.index') }}" class="{{ Route::current()->getName() == 'fitur.transaksi.index' || Route::current()->getName() == 'fitur.transaksi.create' || Route::current()->getName() == 'fitur.transaksi.edit' || Route::current()->getName() == 'fitur.transaksi.show' ? 'active' : '' }}">
          <i class="ti-control-shuffle"></i> <span class="title">Transaksi </span> 
          <span class="badge badge-warning">{{ UserHelper::checkTransaksi() }}</span>
        </a>
      </li>

      <!-- Menu - Report -->
      @if(Auth::user()->level == 1)
        <li class="menu-group">Laporan</li>
        <li class="panel">
          <a href="#laporan" data-toggle="collapse" data-parent="#sidebar-nav-menu" aria-expanded="" class=""><i class="ti-files"></i> <span class="title">Laporan Aset</span> <i class="icon-submenu ti-angle-left"></i></a>
          <div id="laporan" class="collapse  ">
            <ul class="submenu">
              <li><a href="{{ route('laporan.aset.index') }}" class="">Laporan List Aset</a></li>
              <li><a href="{{ route('laporan.mutasi.index') }}" class="">Laporan Mutasi Aset</a></li>
            </ul>
          </div>
        </li>
      @endif
      <!-- Menu - Master -->

    </ul>
    <button type="button" class="btn-toggle-minified" title="Toggle Minified Menu"><i class="ti-arrows-horizontal"></i></button>
  </nav>
</div>