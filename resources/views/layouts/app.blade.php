<!doctype html>
<html lang="en">
<head>
  <title>{{ $page_title }} | {{ env('APP_NAME') }}</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

  <!-- Datatables core css -->
  <link href="{{ asset('assets/plugins/datatables.net-bs4/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- Datatables extensions css -->
  <link href="{{ asset('assets/plugins/datatables.net-buttons-bs4/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/plugins/datatables.net-colreorder-bs4/colreorder.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- Toestr Js -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

  @stack('css')

  <!-- App css -->
  <link href="{{ asset('assets/css/bootstrap-custom.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

  <!-- Favicon -->
  <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
</head>
<body>
  <!-- WRAPPER -->
  <div id="wrapper">

    <!-- NAVBAR -->
    @include('layouts.components.navbar')
    <!-- END NAVBAR -->

    <!-- LEFT SIDEBAR -->
    @include('layouts.components.left-sidebar')
    <!-- END LEFT SIDEBAR -->

    <!-- MAIN -->
    <div class="main" id="app">

      <!-- MAIN CONTENT -->
      <div class="main-content">

        @include('layouts.components.content-heading')

        <div class="container-fluid">
          @include('layouts.components.alert')
          <!-- Default password alert -->
          @if(UserHelper::defaultPasswordCheck())
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
              Anda masih menggunakan password default, silahkan ubah password dengan klik <a href="{{ route('user.ubah-password') }}">disini</a>.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
          <!-- Default password alert -->

          @yield('content')

          @yield('modal')
        </div>
      </div>
      <!-- END MAIN CONTENT -->

      <!-- RIGHT SIDEBAR -->
      @include('layouts.components.right-sidebar')
      <!-- END RIGHT SIDEBAR -->

    </div>
    <!-- END MAIN -->

    <div class="clearfix"></div>
    
    <!-- footer -->
    <footer>
      <div class="container-fluid">
        <p class="copyright">&copy; 2020 <a href="#" target="_blank">{{ env('APP_NAME') }}</a>. All Rights Reserved.</p>
      </div>
    </footer>
    <!-- end footer -->

  </div>
  <!-- END WRAPPER -->

  <!-- Vendor -->
  <script src="{{ asset('assets/js/vendor.min.js') }}"></script>

  <!-- Vue js -->
  <script src="{{ asset('js/vue.js') }}"></script>

  <!-- Datables Core -->
  <script src="{{ asset('assets/plugins/datatables.net/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables.net-bs4/dataTables.bootstrap4.min.js') }}"></script>

  <!-- Datables Extension -->
  <script src="{{ asset('assets/plugins/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/jszip/jszip.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables.net-buttons-bs4/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables.net-colreorder/dataTables.colReorder.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables.net-colreorder-bs4/colReorder.bootstrap4.min.js') }}"></script>

  <!-- Sweetalert 2 -->
  <script src="{{ asset('js/plugin/sweetalert2.js') }}"></script>

  <!-- Toestr Js -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

  <!-- Datatables Init -->
  <script src="{{ asset('assets/js/pages/tables-dynamic.init.min.js') }}"></script>

  @stack('script')

  <!-- App -->
  <script src="{{ asset('assets/js/app.min.js') }}"></script>
</body>
</html>