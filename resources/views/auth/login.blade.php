@extends('layouts.app-auth')

@section('content')
<div class="header">
  <div class="logo text-center"><img src="{{ asset('assets/images/logo.png') }}" alt="Management Aset Logo" width="200px"></div>
  <hr>
  <p class="lead">Login</p>
  @include('layouts.components.alert')
</div>
<form class="form-auth-small" method="POST" action="{{ route('login.auth') }}">
  @csrf
  <div class="form-group">
    <label for="email" class="control-label sr-only">Email</label>
    <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" placeholder="Email" value="{{ old('email') }}" autofocus>

    <!-- Error -->
    @if($errors->has('email'))
      <small class="text-danger">
        {{ $errors->first('email') }}
      </small>
    @endif
  </div>
  <div class="form-group">
    <label for="password" class="control-label sr-only">Password</label>
    <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" id="password-showhide2" placeholder="Password" value="{{ old('password') }}">

    <!-- Error -->
    @if($errors->has('password'))
      <small class="text-danger">
        {{ $errors->first('password') }}
      </small>
    @endif
  </div>
  <div class="form-group">
    <label class="fancy-checkbox element-left custom-bgcolor-blue">
      <input type="checkbox">
      <span class="text-muted">Remember me</span>
    </label>
  </div>
  <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
</form>
@endsection

@push('js')
<!-- Password Strength -->
<script src="{{ asset('assets/plugins/pwstrength-bootstrap/pwstrength-bootstrap.min.js') }}"></script>

<!-- Hide/Show Password -->
<script src="{{ asset('assets/plugins/hideshowpassword/hideshowpassword.min.js') }}"></script>

<!-- Masked Input -->
<script src="{{ asset('assets/plugins/jquery.maskedinput/jquery.maskedinput.js') }}"></script>

<!-- Switchery -->
<script src="{{ asset('assets/plugins/switchery-latest/switchery.min.js') }}"></script>

<!-- Plugins init -->
<script src="{{ asset('assets/js/pages/forms-inputs.init.min.js') }}"></script>
@endpush
