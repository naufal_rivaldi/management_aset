<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransaksiDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi_detail', function (Blueprint $table) {
            $table->dropForeign('transaksi_detail_awal_departemen_id_foreign');
            $table->dropForeign('transaksi_detail_akhir_departemen_id_foreign');
            $table->dropColumn(['awal_departemen_id', 'akhir_departemen_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi_detail', function (Blueprint $table) {
            $table->unsignedBigInteger('awal_departemen_id')->after('aset_id');
            $table->unsignedBigInteger('akhir_departemen_id')->after('awal_departemen_id');

            // Foreign Key
            $table->foreign('awal_departemen_id')->references('id')->on('departemen')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('akhir_departemen_id')->references('id')->on('departemen')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}
