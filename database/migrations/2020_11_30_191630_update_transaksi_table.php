<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi', function (Blueprint $table) {
            $table->date('tanggal_transaksi')->after('id');
            $table->unsignedBigInteger('awal_departemen_id')->after('tanggal_transaksi');
            $table->unsignedBigInteger('akhir_departemen_id')->after('awal_departemen_id');

            // Foreign Key
            $table->foreign('awal_departemen_id')->references('id')->on('departemen')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('akhir_departemen_id')->references('id')->on('departemen')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi', function (Blueprint $table) {
            $table->dropColumn(['tanggal_transaksi', 'awal_departemen_id', 'akhir_departemen_id']);
        });
    }
}
