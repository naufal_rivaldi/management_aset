<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('status')->default(1);
            $table->unsignedBigInteger('transaksi_id');
            $table->unsignedBigInteger('aset_id');
            $table->unsignedBigInteger('awal_departemen_id');
            $table->unsignedBigInteger('akhir_departemen_id');
            $table->timestamps();

            // Foreign Key
            $table->foreign('transaksi_id')->references('id')->on('transaksi')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('aset_id')->references('id')->on('aset')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('awal_departemen_id')->references('id')->on('departemen')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('akhir_departemen_id')->references('id')->on('departemen')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_detail');
    }
}
