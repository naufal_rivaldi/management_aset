<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aset', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode', 10)->unique();
            $table->string('merk', 50)->nullable();
            $table->string('serial', 50)->nullable();
            $table->enum('kondisi', ['baru', 'bekas']);
            $table->date('tanggal_perolehan');
            $table->string('no_bukti', 50)->nullable();
            $table->double('harga')->nullable();
            $table->integer('umur');
            $table->boolean('status')->default(1);
            $table->string('barcode')->nullable();
            $table->unsignedBigInteger('departemen_id');
            $table->timestamps();

            // Foreign Key
            $table->foreign('departemen_id')->references('id')->on('departemen')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aset');
    }
}
