<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function(Blueprint $table){
            $table->dropColumn('level');
        });

        Schema::table('user', function(Blueprint $table){
            $table->enum('level', [1,2,3])->default(3)->after('pic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function(Blueprint $table){
            $table->dropColumn('level');
        });

        Schema::table('user', function(Blueprint $table){
            $table->enum('level', [1,2])->default(2)->after('pic');
        });
    }
}
