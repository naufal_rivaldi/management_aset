<?php

use Illuminate\Database\Seeder;

use App\Models\Departemen;

class DepartemenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataArray = [
            [
                "nama"      => "General Affair",
                "no_telp"   => "0819xxxxxxxx",
                "alamat"    => "Jln. Teuku Umar Barat 1"
            ],
            [
                "nama"      => "IT and Adprom",
                "no_telp"   => "0829xxxxxxxx",
                "alamat"    => "Jln. Teuku Umar Barat 1"
            ],
            [
                "nama"      => "Accounting",
                "no_telp"   => "0839xxxxxxxx",
                "alamat"    => "Jln. Teuku Umar Barat 1"
            ],
        ];

        Departemen::insert($dataArray);
    }
}
