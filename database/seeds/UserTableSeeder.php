<?php

use Illuminate\Database\Seeder;

use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "email"         => "admin@citrawarna.com",
            "password"      => bcrypt('12345'),
            "pic"           => "Admin",
            "level"         => 1,
            "departemen_id" => 1,
        ]);
    }
}
