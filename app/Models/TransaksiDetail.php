<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransaksiDetail extends Model
{
    protected $table = 'transaksi_detail';
    protected $guarded = [];

    // Relationship
    public function aset(){
        return $this->belongsTo('App\Models\Aset', 'aset_id');
    }

    public function transaksi(){
        return $this->belongsTo('App\Models\Transaksi', 'transaksi_id');
    }
}
