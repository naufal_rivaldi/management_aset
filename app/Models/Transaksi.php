<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';
    protected $guarded = [];

    // Relationship
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function transaksi_detail(){
        return $this->hasMany('App\Models\TransaksiDetail', 'transaksi_id');
    }

    public function departemen_awal(){
        return $this->belongsTo('App\Models\Departemen', 'awal_departemen_id');
    }

    public function departemen_akhir(){
        return $this->belongsTo('App\Models\Departemen', 'akhir_departemen_id');
    }
}
