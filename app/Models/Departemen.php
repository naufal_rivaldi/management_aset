<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departemen extends Model
{
    protected $table = 'departemen';
    protected $guarded = [];

    public $timestamps = false;

    // Relationship
    public function user(){
        return $this->hasMany('App\User', 'departemen_id');
    }

    public function transaksi_awal(){
        return $this->hasMany('App\Models\Transaksi', 'awal_departemen_id');
    }

    public function transaksi_akhir(){
        return $this->hasMany('App\Models\Transaksi', 'akhir_departemen_id');
    }
}
