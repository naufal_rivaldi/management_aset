<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aset extends Model
{
    protected $table = 'aset';
    protected $guarded = [];

    // Relationship
    public function departemen(){
        return $this->belongsTo('App\Models\Departemen', 'departemen_id');
    }

    public function transaksi_detail(){
        return $this->hasMany('App\Models\TransaksiDetail', 'transaksi_id');
    }
}
