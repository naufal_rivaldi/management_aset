<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

use App\Models\Transaksi;
use App\Models\Aset;

use Auth;
use Hash;

class UserHelper {
    public static function defaultPasswordCheck() {
        $default_password = '12345';

        if(Hash::check($default_password, Auth::user()->password)){
          return true;
        }

        return false;
    }

    public static function checkTransaksi(){
      $departemen_id = Auth::user()->departemen->id;
      $transaksi = Transaksi::where('akhir_departemen_id', $departemen_id)->where('verifikasi_status', 0)->get();

      return count($transaksi);
    }

    public static function checkDataAset(){
      $aset = Aset::with('departemen')->where('status', 1)->where('aset_check', 0)->get();

      return count($aset);
    }
}