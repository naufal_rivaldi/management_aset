<?php

// =======================================
// Parse data
// =======================================
function parseJson($array){
  return str_replace("'","`", json_encode($array));
}
// =======================================

// =======================================
// Boolean Data
// =======================================
function status($val){
  if($val == 1){
    return '<span class="badge badge-success">Active</span>';
  }else{
    return '<span class="badge badge-secondary">Inactive</span>';
  }
}

function statusButton($val, $id){
  if($val == 1){
    return '<button class="btn btn-success btn-inactive" data-id="'.$id.'">Active</button>';
  }else{
    return '<button class="btn btn-secondary btn-active" data-id="'.$id.'">Inactive</button>';
  }
}

function statusAset($val){
  if($val == 1){
    return '<span class="badge badge-success">Aset Baru</span>';
  }else{
    return '<span class="badge badge-secondary">Aset Lama</span>';
  }
}

function statusVerifikasi($val){
  if($val == 1){
    return '<span class="badge badge-success">Terverifikasi</span>';
  }else{
    return '<span class="badge badge-secondary">Butuh Verifikasi</span>';
  }
}

function level($val){
  if($val == 1){
    return '<span class="badge badge-success">Admin</span>';
  }elseif($val == 2){
    return '<span class="badge badge-info">Editor</span>';
  }else{
    return '<span class="badge badge-secondary">Pengguna</span>';
  }
}
// =======================================

// =======================================
// Date Format Data
// =======================================
function longDateFormat($date){
  return date('l, d F Y', strtotime($date));
}
// =======================================
