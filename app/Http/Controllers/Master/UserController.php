<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Models\Departemen;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'User';
        $data['filtering'] = (object)[
            'level' => ['Admin', 'Editor', 'User'],
            'status' => ['Active', 'Inactive'],
            'departements' => Departemen::pluck('nama'),
        ];

        return view('pages.master.user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Tambah User';
        $data['departemens'] = Departemen::all();
        $data['user'] = (object)[
            'id' => '',
            'email' => '',
            'pic' => '',
            'level' => '',
            'departemen_id' => '',
        ];

        return view('pages.master.user.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'pic' => 'required|string|min:3',
            'level' => 'required|numeric',
            'departemen_id' => 'required|numeric'
        ])->validate();

        User::create([
            'email' => $request->email,
            'password' => bcrypt('12345'),
            'pic' => $request->pic,
            'level' => $request->level,
            'status' => 1,
            'departemen_id' => $request->departemen_id
        ]);

        return redirect()->route('master.user.index')->with('success', 'Simpan data berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    // Data json
    public function json($param){
        switch ($param) {
            case 'datatable':
                $users = User::with('departemen')->select('user.*');

                return datatables()->of($users)
                                    ->addIndexColumn()
                                    ->addColumn('level', function($row){
                                        return level($row->level);
                                    })
                                    ->addColumn('status', function($row){
                                        return statusButton($row->status, $row->id);
                                    })
                                    ->addColumn('action', function($row){
                                        $button = '<div class="btn-group" role="group" aria-label="Basic example">';
                                        $button .= '<button class="btn btn-sm btn-success reset-password" data-id="'.$row->id.'"><i class="ti-key"></i></button>';
                                        $button .= '<a href="'.route('master.user.edit', $row->id).'" class="btn btn-sm btn-warning"><i class="ti-settings"></i></a>';
                                        $button .= '<button type="button" data-id="'.$row->id.'" class="btn btn-sm btn-danger delete-data"><i class="ti-trash"></i></button>';
                                        $button .= '</div>';
                    
                                        return $button;
                                    })
                                    ->filterColumn('level', function($query,$keyword){
                                        $val = 1;
                                        if($keyword == 'Editor'){
                                            $val = 2;
                                        }elseif($keyword == 'User'){
                                            $val = 3;
                                        }

                                        $query->where('level', $val);
                                    })
                                    ->filterColumn('status', function($query,$keyword){
                                        $val = 1;
                                        if($keyword == 'Inactive'){
                                            $val = 0;
                                        }

                                        $query->where('status', $val);
                                    })
                                    ->rawColumns(['level', 'status', 'action'])
                                    ->make(true);
                break;
            
            default:
                # code...
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit User';
        $data['departemens'] = Departemen::all();
        $data['user'] = User::find($id);

        return view('pages.master.user.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'pic' => 'required|string|min:3',
            'level' => 'required|numeric',
            'status' => 'required|numeric',
            'departemen_id' => 'required|numeric'
        ])->validate();

        $user = User::find($id);
        $user->email = $request->email;
        $user->pic = $request->pic;
        $user->level = $request->level;
        $user->status = $request->status;
        $user->departemen_id = $request->departemen_id;
        $user->save();

        return redirect()->route('master.user.index')->with('success', 'Update data berhasil!');
    }

    public function active($id){
        $user = User::find($id);
        $user->status = 1;
        $user->save();
    }

    public function inactive($id){
        $user = User::find($id);
        $user->status = 0;
        $user->save();
    }

    public function resetPassword(Request $request, $id){
        $user = User::find($id);
        $user->password = bcrypt('12345');
        $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
    }
}
