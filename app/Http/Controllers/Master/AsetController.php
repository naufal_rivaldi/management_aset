<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
// use SimpleSoftwareIO\QrCode\Facades\QrCode;

use App\Models\Aset;
use App\Models\Departemen;

use Auth;
use QRCode;

class AsetController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Aset';
        $data['filtering'] = (object)[
            'kondisi' => ['Baru', 'Bekas'],
            'status' => ['Active', 'Inactive'],
        ];

        return view('pages.master.aset.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Tambah Aset';
        $data['departemens'] = Departemen::all();
        $data['aset'] = (object)[
            'id' => '',
            'kode' => '',
            'merk' => '',
            'nama' => '',
            'serial' => '',
            'tanggal_perolehan' => date('Y-m-d'),
            'kondisi' => '',
            'deskripsi' => '',
            'departemen_id' => '',
        ];

        return view('pages.master.aset.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode' => 'required|string|unique:aset,kode',
            'nama' => 'required|string|min:3',
            'tanggal_perolehan' => 'required|date',
            'kondisi' => 'required',
            'departemen_id' => 'required'
        ])->validate();

        Aset::create([
            'kode' => $request->kode,
            'merk' => $request->merk,
            'nama' => $request->nama,
            'serial' => $request->serial,
            'kondisi' => $request->kondisi,
            'deskripsi' => $request->deskripsi,
            'tanggal_perolehan' => $request->tanggal_perolehan,
            'status' => 1,
            'departemen_id' => $request->departemen_id
        ]);

        return redirect()->route('master.aset.index')->with('success', 'Simpan data berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aset = Aset::find($id);
        $data['page_title'] = 'Detail Aset';
        $data['aset'] = $aset;
        $data['depresiasi'] = null;

        // Qrcode
        $path = public_path('img/qr-code/'.$aset->kode.'.png');
        QRCode::text(route('cari.aset', $aset->kode))->setOutfile($path)->png();

        // =================================================
        // Get Penyusutan Data
        // =================================================
        if($aset->harga != null || $aset->nilai_residu != null || $aset->umur != null){
            $result = 0;
            $data_depresiasi = [];
            $between_month = $this->getMonth($aset->tanggal_perolehan);
            $year = date('Y', strtotime($aset->tanggal_perolehan));
            $end_year = date('Y', strtotime($aset->tanggal_perolehan."+".$aset->umur." year"));

            if($between_month[0] != 12){
                while ($year <= $end_year) {
                    $result = 0;
                    $total_month = 0;
                    if($between_month[0] != null){
                        $result = $between_month[0]/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                        $total_month = $between_month[0];
                        $between_month[0] = null;
                    }elseif($year == $end_year){
                        $result = $between_month[1]/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                        $total_month = $between_month[1];
                    }else{
                        $result = 12/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                        $total_month = 12;
                    }

                    $data_depresiasi[] = (object)[
                        "year" => $year,
                        "month" => $total_month,
                        "depresiasi" => ceil($result),
                    ];
                    
                    $year += 1;
                }
            }else{
                $end_year -= 1;
                while ($year <= $end_year) {
                    $result = 0;
                    $total_month = 0;
                    $result = 12/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                    $total_month = 12;

                    $data_depresiasi[] = (object)[
                        "year" => $year,
                        "month" => $total_month,
                        "depresiasi" => ceil($result),
                    ];
                    
                    $year += 1;
                }
            }
            
            $data['depresiasi'] = $data_depresiasi;
        }
        // =================================================

        return view('pages.master.aset.show', $data);
    }

    // Data json
    public function json($param){
        switch ($param) {
            case 'datatable':
                if(Auth::user()->level == 1){
                    $aset = Aset::with('departemen');
                }else{
                    $aset = Aset::with('departemen')->where('departemen_id', Auth::user()->departemen_id);
                }

                return datatables()->of($aset)
                                    ->addIndexColumn()
                                    ->addColumn('status', function($row){
                                        return statusButton($row->status, $row->id);
                                    })
                                    ->addColumn('action', function($row){
                                        $button = '<div class="btn-group" role="group" aria-label="Basic example">';
                                        $button .= '<a href="'.route('master.aset.show', $row->id).'" class="btn btn-sm btn-info"><i class="ti-eye"></i></a>';

                                        if(Auth::user()->level == 1){
                                            $button .= '<a href="'.route('master.aset.edit', $row->id).'" class="btn btn-sm btn-warning"><i class="ti-settings"></i></a>';
                                            $button .= '<button type="button" data-id="'.$row->id.'" class="btn btn-sm btn-danger delete-data"><i class="ti-trash"></i></button>';
                                        }
                                        
                                        $button .= '</div>';
                    
                                        return $button;
                                    })
                                    ->filterColumn('status', function($query,$keyword){
                                        $val = 1;
                                        if($keyword == 'Inactive'){
                                            $val = 0;
                                        }

                                        $query->where('status', $val);
                                    })
                                    ->rawColumns(['status', 'action'])
                                    ->make(true);
                break;
            
            default:
                # code...
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit Aset';
        $data['departemens'] = Departemen::all();
        $data['aset'] = Aset::find($id);

        return view('pages.master.aset.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'kode' => 'required|string|unique:aset,kode,'.$id,
            'nama' => 'required|string|min:3',
            'tanggal_perolehan' => 'required|date',
            'kondisi' => 'required',
            'departemen_id' => 'required'
        ])->validate();

        $aset = Aset::find($id);
        $aset->kode = $request->kode;
        $aset->merk = $request->merk;
        $aset->nama = $request->nama;
        $aset->serial = $request->serial;
        $aset->kondisi = $request->kondisi;
        $aset->deskripsi = $request->deskripsi;
        $aset->tanggal_perolehan = $request->tanggal_perolehan;
        $aset->departemen_id = $request->departemen_id;
        $aset->save();

        return redirect()->route('master.aset.index')->with('success', 'Update data berhasil!');
    }

    public function active($id){
        $aset = Aset::find($id);
        $aset->status = 1;
        $aset->save();
    }

    public function inactive($id){
        $aset = Aset::find($id);
        $aset->status = 0;
        $aset->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aset = Aset::find($id);
        $aset->delete();
    }

    private function getMonth($date){
        $result = [];
        
        // First month
        $month = date('m', strtotime($date));
        $month = 13 - $month;
        $result[] = $month;

        // Last month
        $month = date('m', strtotime($date));
        $month = $month - 1;
        $result[] = $month;

        return $result;
    }
}
