<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Departemen;

class DepartemenController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Departemen';

        return view('pages.master.departemen.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Tambah Departemen';
        $data['departemen'] = (object)[
            'id' => '',
            'nama' => '',
            'no_telp' => '',
            'alamat' => '',
        ];

        return view('pages.master.departemen.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:100|min:3',
        ])->validate();

        Departemen::create([
            'nama' => $request->nama,
            'no_telp' => $request->no_telp,
            'alamat' => $request->alamat,
        ]);

        return redirect()->route('master.departemen.index')->with('success', 'Simpan data berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    // Data json
    public function json($param){
        switch ($param) {
            case 'datatable':
                $Departements = Departemen::query();

                return datatables()->of($Departements)
                                    ->addIndexColumn()
                                    ->addColumn('action', function($row){
                                        $button = '<div class="btn-group" role="group" aria-label="Basic example">';
                                        $button .= '<a href="'.route('master.departemen.edit', $row->id).'" class="btn btn-sm btn-warning"><i class="ti-settings"></i></a>';
                                        $button .= '<button type="button" data-id="'.$row->id.'" class="btn btn-sm btn-danger delete-data"><i class="ti-trash"></i></button>';
                                        $button .= '</div>';
                    
                                        return $button;
                                    })
                                    ->rawColumns(['action'])
                                    ->make(true);
                break;
            
            default:
                # code...
                break;
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit Departemen';
        $data['departemen'] = Departemen::find($id);

        return view('pages.master.departemen.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:100|min:3',
        ])->validate();

        $departemen = Departemen::find($id);
        $departemen->nama = $request->nama;
        $departemen->no_telp = $request->no_telp;
        $departemen->alamat = $request->alamat;
        $departemen->save();

        return redirect()->route('master.departemen.index')->with('success', 'Update data berhasil!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $departemen = Departemen::find($id);
        $departemen->delete();
    }
}
