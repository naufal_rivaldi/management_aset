<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Aset;

class CariController extends Controller
{
    public function index($kode){
        $aset = Aset::where('kode', $kode)->first();
        $data['aset'] = $aset;
        $data['depresiasi'] = null;

        // =================================================
        // Get Penyusutan Data
        // =================================================
        if($aset->harga != null || $aset->nilai_residu != null || $aset->umur != null){
            $result = 0;
            $data_depresiasi = [];
            $between_month = $this->getMonth($aset->tanggal_perolehan);
            $year = date('Y', strtotime($aset->tanggal_perolehan));
            $end_year = date('Y', strtotime($aset->tanggal_perolehan."+".$aset->umur." year"));

            if($between_month[0] != 12){
                while ($year <= $end_year) {
                    $result = 0;
                    $total_month = 0;
                    if($between_month[0] != null){
                        $result = $between_month[0]/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                        $total_month = $between_month[0];
                        $between_month[0] = null;
                    }elseif($year == $end_year){
                        $result = $between_month[1]/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                        $total_month = $between_month[1];
                    }else{
                        $result = 12/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                        $total_month = 12;
                    }

                    $data_depresiasi[] = (object)[
                        "year" => $year,
                        "month" => $total_month,
                        "depresiasi" => ceil($result),
                    ];
                    
                    $year += 1;
                }
            }else{
                $end_year -= 1;
                while ($year <= $end_year) {
                    $result = 0;
                    $total_month = 0;
                    $result = 12/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                    $total_month = 12;

                    $data_depresiasi[] = (object)[
                        "year" => $year,
                        "month" => $total_month,
                        "depresiasi" => ceil($result),
                    ];
                    
                    $year += 1;
                }
            }
            
            $data['depresiasi'] = $data_depresiasi;
        }
        // =================================================

        return view('pages.cari.index', $data);
    }

    private function getMonth($date){
        $result = [];
        
        // First month
        $month = date('m', strtotime($date));
        $month = 13 - $month;
        $result[] = $month;

        // Last month
        $month = date('m', strtotime($date));
        $month = $month - 1;
        $result[] = $month;

        return $result;
    }
}
