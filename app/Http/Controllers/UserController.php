<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\User;

use Auth;
use Hash;

class UserController extends Controller
{
    // ====================================================
    // Ubah password view - function
    // ====================================================
    public function ubahPassword(){
        $data['page_title'] = "Ubah Password";

        return view('pages.user.ubah-password', $data);
    }

    // ====================================================
    // Ubah password store - function
    // ====================================================
    public function storeUbahPassword(Request $request){
        $validator = Validator::make($request->all(), [
            "old_password" => "required",
            "new_password" => "required",
            "confirm_password" => "required|same:new_password",
        ])->validate();

        $user = User::find(Auth::user()->id);
        
        if(Hash::check($request->old_password, $user->password)){
            $user->password = bcrypt($request->new_password);
            $user->save();

            Auth::logout();
            return redirect()->route('login')->with('info', 'Password berhasil diubah, silahkan login kembali.');
        }

        return redirect()->route('user.ubah-password')->with('danger', 'Password tidak valid!');
    }
}
