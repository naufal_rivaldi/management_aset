<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Aset;
use App\Models\Transaksi;

use Auth;

class LaporanMutasiController extends Controller
{
    public function index(){
        $data['page_title'] = 'Laporan List Mutasi';
        $data['filtering'] = (object)[
            'verifikasi' => ['Terverifikasi', 'Butuh Verifikasi'],
            'status' => ['Aset Baru', 'Aset Lama'],
        ];

        return view('pages.laporan.mutasi.index', $data);
    }

    // Data json
    public function json($param){
        switch ($param) {
            case 'datatable':
                $transaksi = Transaksi::with(['transaksi_detail', 'departemen_awal', 'departemen_akhir'])->orderBy('tanggal_transaksi', 'desc');

                return datatables()->of($transaksi)
                                    ->addIndexColumn()
                                    ->addColumn('verifikasi_status', function($row){
                                        return statusVerifikasi($row->verifikasi_status);
                                    })
                                    ->addColumn('list_aset', function($row){
                                        $text = "<ul>";

                                        foreach($row->transaksi_detail as $row){
                                            $text .= "<li>".$row->aset->kode.'/'.$row->aset->nama."</li>";
                                        }

                                        $text .= "</ul>";
                                        
                                        return $text;
                                    })
                                    ->addColumn('status', function($row){
                                        return statusAset($row->status);
                                    })
                                    ->filterColumn('verifikasi_status', function($query,$keyword){
                                        $val = 1;
                                        if($keyword == 'Butuh Verifikasi'){
                                            $val = 0;
                                        }

                                        $query->where('verifikasi_status', $val);
                                    })
                                    ->filterColumn('status', function($query,$keyword){
                                        $val = 1;
                                        if($keyword == 'Aset Lama'){
                                            $val = 2;
                                        }

                                        $query->where('status', $val);
                                    })
                                    ->rawColumns(['verifikasi_status', 'list_aset', 'status'])
                                    ->make(true);
                break;
            
            default:
                # code...
                break;
        }
    }
}
