<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Aset;

use Auth;

class LaporanAsetController extends Controller
{
    public function index(){
        $data['page_title'] = 'Laporan List Aset';
        $data['filtering'] = (object)[
            'kondisi' => ['Baru', 'Bekas'],
            'status' => ['Active', 'Inactive'],
        ];

        return view('pages.laporan.aset.index', $data);
    }

    // Data json
    public function json($param){
        switch ($param) {
            case 'datatable':
                if(Auth::user()->level == 1){
                    $aset = Aset::with('departemen');
                }else{
                    $aset = Aset::with('departemen')->where('departemen_id', Auth::user()->departemen_id);
                }

                return datatables()->of($aset)
                                    ->addIndexColumn()
                                    ->addColumn('status', function($row){
                                        return status($row->status);
                                    })
                                    ->addColumn('action', function($row){
                                        $button = '<div class="btn-group" role="group" aria-label="Basic example">';
                                        $button .= '<a href="'.route('master.aset.show', $row->id).'" class="btn btn-sm btn-info"><i class="ti-eye"></i></a>';
                                        $button .= '<a href="'.route('master.aset.edit', $row->id).'" class="btn btn-sm btn-warning"><i class="ti-settings"></i></a>';
                                        $button .= '<button type="button" data-id="'.$row->id.'" class="btn btn-sm btn-danger delete-data"><i class="ti-trash"></i></button>';
                                        $button .= '</div>';
                    
                                        return $button;
                                    })
                                    ->filterColumn('status', function($query,$keyword){
                                        $val = 1;
                                        if($keyword == 'Inactive'){
                                            $val = 0;
                                        }

                                        $query->where('status', $val);
                                    })
                                    ->rawColumns(['status', 'action'])
                                    ->make(true);
                break;
            
            default:
                # code...
                break;
        }
    }
}
