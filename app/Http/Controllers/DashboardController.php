<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Models\Departemen;
use App\Models\Aset;

use Auth;

class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $data['page_title'] = "Dashboard";
        $data['user'] = User::where('status', 1)->get();
        $data['departemen'] = Departemen::all();

        if(Auth::user()->level > 1){
            $data['aset'] = Aset::where('status', 1)->where('departemen_id', Auth::user()->departemen_id)->get();
        }else{
            $data['aset'] = Aset::where('status', 1)->get();
        }
        
        return view('pages.dashboard.index', $data);
    }
}
