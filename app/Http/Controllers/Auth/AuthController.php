<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;

class AuthController extends Controller
{
    // ====================================================
    // Logout - function
    // ====================================================
    public function logout(){
        Auth::logout();

        return redirect()->route('login');
    }
}
