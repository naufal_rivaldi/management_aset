<?php

namespace App\Http\Controllers\Fitur;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Aset;
use App\Models\Departemen;
use App\Models\Transaksi;
use App\Models\TransaksiDetail;

use Auth;

class TransaksiController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Transaksi / Mutasi Aset';
        $data['filtering'] = (object)[
            'verifikasi' => ['Terverifikasi', 'Butuh Verifikasi'],
            'status' => ['Aset Baru', 'Aset Lama'],
        ];

        return view('pages.fitur.transaksi.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Tambah Transaksi / Mutasi Aset';
        $data['departemens'] = Departemen::all();
        $data['asets'] = Aset::where('status', 1)->get();
        $data['transaksi'] = (object)[
            'id' => '',
            'deskripsi' => '',
            'kondisi' => '',
            'status' => '',
            'user_id' => '',
            'transaksi_detail' => '',
            'akhir_departemen_id' => '',
            'awal_departemen_id' => '',
            'tanggal_transaksi' => date('Y-m-d'),
        ];

        return view('pages.fitur.transaksi.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tanggal_transaksi' => 'required|date',
            'awal_departemen_id' => 'required',
            'akhir_departemen_id' => 'required',
            'aset_id' => 'required|array',
            'aset_id.*' => 'required',
            'status' => 'required'
        ])->validate();

        $transaksi = Transaksi::create([
            'tanggal_transaksi' => $request->tanggal_transaksi,
            'awal_departemen_id' => $request->awal_departemen_id,
            'akhir_departemen_id' => $request->akhir_departemen_id,
            'deskripsi' => $request->deskripsi,
            'status' => $request->status,
            'user_id' => Auth::user()->id,
        ]);

        for($i=0; $i<count($request->aset_id); $i++){
            TransaksiDetail::create([
                'transaksi_id' => $transaksi->id,
                'aset_id' => $request->aset_id[$i],
            ]);
        }

        return redirect()->route('fitur.transaksi.index')->with('success', 'Transaksi berhasil!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['page_title'] = 'Detail Transaksi / Mutasi Aset';
        $data['transaksi'] = Transaksi::find($id);

        return view('pages.fitur.transaksi.show', $data);
    }

    // Data json
    public function json($param){
        switch ($param) {
            case 'datatable':
                if(Auth::user()->level == 1){
                    $transaksi = Transaksi::with(['user', 'departemen_awal', 'departemen_akhir']);
                }else{
                    $transaksi = Transaksi::with(['user', 'departemen_awal', 'departemen_akhir'])->where('akhir_departemen_id', Auth::user()->departemen->id);
                }

                return datatables()->of($transaksi)
                                    ->addIndexColumn()
                                    ->addColumn('verifikasi_status', function($row){
                                        return statusVerifikasi($row->verifikasi_status);
                                    })
                                    ->addColumn('status', function($row){
                                        return statusAset($row->status);
                                    })
                                    ->addColumn('action', function($row){
                                        $button = '<div class="btn-group" role="group" aria-label="Basic example">';
                                        $button .= '<a href="'.route('fitur.transaksi.show', $row->id).'" class="btn btn-sm btn-info"><i class="ti-eye"></i></a>';

                                        if(Auth::user()->level == 1){
                                            $button .= '<a href="'.route('fitur.transaksi.edit', $row->id).'" class="btn btn-sm btn-warning" '.($row->verifikasi_status == 1 ? 'disabled' : '').'><i class="ti-settings"></i></a>';
                                            $button .= '<button type="button" data-id="'.$row->id.'" class="btn btn-sm btn-danger delete-data" '.($row->verifikasi_status == 1 ? 'disabled' : '').'><i class="ti-trash"></i></button>';
                                        }

                                        $button .= '</div>';
                    
                                        return $button;
                                    })
                                    ->filterColumn('verifikasi_status', function($query,$keyword){
                                        $val = 1;
                                        if($keyword == 'Butuh Verifikasi'){
                                            $val = 0;
                                        }

                                        $query->where('verifikasi_status', $val);
                                    })
                                    ->filterColumn('status', function($query,$keyword){
                                        $val = 1;
                                        if($keyword == 'Aset Lama'){
                                            $val = 0;
                                        }

                                        $query->where('status', $val);
                                    })
                                    ->rawColumns(['verifikasi_status', 'status', 'action'])
                                    ->make(true);
                break;

            case 'aset':
                $departemen_id = $_GET['departemen_id'];
                $aset = Aset::where('status', 1)->where('departemen_id', $departemen_id)->get();

                return response()->json($aset);
                break;
            
            default:
                # code...
                break;
        }
    }

    public function getAset($id){
        $aset = Aset::with('departemen')->find($id);

        return response()->json($aset);
    }

    public function checkAset(Request $request, $id){
        $transaksi_detail = TransaksiDetail::find($id);
        $transaksi_detail->status = $request->type;
        $transaksi_detail->save();

        return response()->json([
            'message' => 'Check aset berhasil!'
        ]);
    }

    public function verifikasi($id){
        $transaksi = Transaksi::find($id);
        
        $idx = 0;
        foreach($transaksi->transaksi_detail as $row){
            if($row->status == 1){
                $idx += 1;
            }
        }

        if($idx > 0){
            foreach($transaksi->transaksi_detail as $row){
                if($row->status == 1){
                    $aset = Aset::find($row->aset_id);
                    $aset->departemen_id = $transaksi->akhir_departemen_id;
                    $aset->save();
                }
            }
            
            $transaksi->verifikasi_status = 1;
            $transaksi->save();

            return response()->json([
                'status' => true,
            ]);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Tidak ada aset yang sudah di cek!'
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit Transaksi / Mutasi Aset';
        $data['departemens'] = Departemen::all();
        $data['asets'] = Aset::where('status', 1)->get();
        $data['transaksi'] = Transaksi::find($id);

        return view('pages.fitur.transaksi.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'tanggal_transaksi' => 'required|date',
            'awal_departemen_id' => 'required',
            'akhir_departemen_id' => 'required',
            'aset_id' => 'required|array',
            'aset_id.*' => 'required',
            'status' => 'required'
        ])->validate();

        $transaksi = Transaksi::find($id);
        $transaksi->tanggal_transaksi = $request->tanggal_transaksi;
        $transaksi->awal_departemen_id = $request->awal_departemen_id;
        $transaksi->akhir_departemen_id = $request->akhir_departemen_id;
        $transaksi->deskripsi = $request->deskripsi;
        $transaksi->status = $request->status;
        $transaksi->user_id = Auth::user()->id;
        $transaksi->save();
        
        TransaksiDetail::where('transaksi_id', $id)->delete();
        for($i=0; $i<count($request->aset_id); $i++){
            TransaksiDetail::create([
                'transaksi_id' => $transaksi->id,
                'aset_id' => $request->aset_id[$i],
            ]);
        }

        return redirect()->route('fitur.transaksi.index')->with('success', 'Update data berhasil!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaksi = Transaksi::find($id);
        $transaksi->delete();
    }
}
