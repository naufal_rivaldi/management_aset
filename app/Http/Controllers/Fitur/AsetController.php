<?php

namespace App\Http\Controllers\Fitur;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Aset;
use App\Models\Departemen;

class AsetController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Data Aset';
        $data['filtering'] = (object)[
            'kondisi' => ['Baru', 'Bekas'],
            'status' => ['Active', 'Inactive'],
        ];

        return view('pages.fitur.aset.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aset = Aset::find($id);
        $data['page_title'] = 'Detail Data Aset';
        $data['aset'] = $aset;

        // =================================================
        // Get Penyusutan Data
        // =================================================
        if($aset->harga != null || $aset->nilai_residu != null || $aset->umur != null){
            $result = 0;
            $data_depresiasi = [];
            $between_month = $this->getMonth($aset->tanggal_perolehan);
            $year = date('Y', strtotime($aset->tanggal_perolehan));
            $end_year = date('Y', strtotime($aset->tanggal_perolehan."+".$aset->umur." year"));

            if($between_month[0] != 12){
                while ($year <= $end_year) {
                    $result = 0;
                    $total_month = 0;
                    if($between_month[0] != null){
                        $result = $between_month[0]/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                        $total_month = $between_month[0];
                        $between_month[0] = null;
                    }elseif($year == $end_year){
                        $result = $between_month[1]/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                        $total_month = $between_month[1];
                    }else{
                        $result = 12/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                        $total_month = 12;
                    }

                    $data_depresiasi[] = (object)[
                        "year" => $year,
                        "month" => $total_month,
                        "depresiasi" => ceil($result),
                    ];
                    
                    $year += 1;
                }
            }else{
                $end_year -= 1;
                while ($year <= $end_year) {
                    $result = 0;
                    $total_month = 0;
                    $result = 12/12 * ($aset->harga - $aset->nilai_residu) / $aset->umur;
                    $total_month = 12;

                    $data_depresiasi[] = (object)[
                        "year" => $year,
                        "month" => $total_month,
                        "depresiasi" => ceil($result),
                    ];
                    
                    $year += 1;
                }
            }
            $data['depresiasi'] = $data_depresiasi;
            
        }

        return view('pages.fitur.aset.show', $data);
    }

    // Data json
    public function json($param){
        switch ($param) {
            case 'datatable1':
                $aset = Aset::with('departemen')->where('status', 1)->where('aset_check', 1);

                return datatables()->of($aset)
                                    ->addIndexColumn()
                                    ->addColumn('action', function($row){
                                        $button = '<div class="btn-group" role="group" aria-label="Basic example">';
                                        $button .= '<a href="'.route('fitur.aset.show', $row->id).'" class="btn btn-sm btn-info"><i class="ti-eye"></i></a>';
                                        $button .= '</div>';
                    
                                        return $button;
                                    })
                                    ->rawColumns(['action'])
                                    ->make(true);
                break;

            case 'datatable2':
                $aset = Aset::with('departemen')->where('status', 1)->where('aset_check', 0);

                return datatables()->of($aset)
                                    ->addIndexColumn()
                                    ->addColumn('action', function($row){
                                        $button = '<div class="btn-group" role="group" aria-label="Basic example">';
                                        $button .= '<a href="'.route('fitur.aset.show', $row->id).'" class="btn btn-sm btn-info"><i class="ti-eye"></i></a>';
                                        $button .= '</div>';
                    
                                        return $button;
                                    })
                                    ->rawColumns(['action'])
                                    ->make(true);
                break;
            
            default:
                # code...
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'no_bukti' => 'required',
            'harga' => 'required|numeric',
            'nilai_residu' => 'required|numeric',
            'umur' => 'required|numeric',
        ])->validate();

        $aset = Aset::find($id);
        $aset->no_bukti = $request->no_bukti;
        $aset->harga = $request->harga;
        $aset->nilai_residu = $request->nilai_residu;
        $aset->umur = $request->umur;
        $aset->aset_check = empty($request->aset_check) ? 0 : 1;
        $aset->save();

        return redirect()->route('fitur.aset.index')->with('success', 'Update data berhasil!');
    }

    public function ubahData($id){
        $aset = Aset::find($id);
        $aset->aset_check = 0;
        $aset->save();

        return redirect()->route('fitur.aset.show', $id)->with('info', 'Silahkan update data kembali.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getMonth($date){
        $result = [];
        
        // First month
        $month = date('m', strtotime($date));
        $month = 13 - $month;
        $result[] = $month;

        // Last month
        $month = date('m', strtotime($date));
        $month = $month - 1;
        $result[] = $month;

        return $result;
    }
}
